/*
 * Copyright (c) 2021 Lucas Mancini <mancinilucas95@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @file	api_pid.cpp
 * @date	2021-19-MAY
 * @author:
 *  - LM		Lucas Mancini <mancinilucas95@gmail.com>.
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/
#include "include/api_pid.h"

/********************** macros and definitions *******************************/

#define API_PID_TEMPERATURE_KP (1)
#define API_PID_TEMPERATURE_KI (1)
#define API_PID_TEMPERATURE_KD (1)

#define API_PID_TEMPERATURE_PON BOARD_PID_PROPORTIONAL_ON_MEASUREMENT
#define API_PID_TEMPERATURE_DIRECTION BOARD_PID_DIRECT_FLAG

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

static struct{

	bool enable[API_PID_TAG__N];
	board_pid_t pid[API_PID_TAG__N];
	double input[API_PID_TAG__N];
	double output[API_PID_TAG__N];
	double setpoint[API_PID_TAG__N];

}self_;


/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

void api_pid_automatic_mode(api_pid_tag_t pid_tag)
{
	board_pid_SetMode(&self_.pid[pid_tag], BOARD_PID_AUTOMATIC_FLAG);
}
void api_pid_manual_mode(api_pid_tag_t pid_tag)
{
	board_pid_SetMode(&self_.pid[pid_tag], BOARD_PID_MANUAL_FLAG);
}

void api_pid_set_output_limits(api_pid_tag_t pid_tag, double min, double max)
{
	board_pid_SetOutputLimits(&self_.pid[pid_tag],min,max);
}


void api_pid_start(api_pid_tag_t pid_tag, double input_o, double setpoint_o)
{
	self_.enable[pid_tag]=true;
	self_.input[pid_tag]=input_o;
	self_.setpoint[pid_tag]=setpoint_o;
}

void api_pid_stop(api_pid_tag_t pid_tag)
{

	self_.enable[pid_tag]=false;

}

double api_pid_get_output(api_pid_tag_t pid_tag)
{

	return self_.output[pid_tag];

}

void api_pid_set_input(api_pid_tag_t pid_tag, double input){

	self_.input[pid_tag] = input;

}

void api_pid_change_setpoint(api_pid_tag_t pid_tag, double setpoint){

	self_.setpoint[pid_tag] = setpoint;

}

double api_pid_get_setpoint(api_pid_tag_t pid_tag){

	return self_.setpoint[pid_tag];

}

void api_pid_set_tunning_constants(api_pid_tag_t pid_tag, double Kp, double Ki, double Kd){

	board_pid_SetTunings_simple(&self_.pid[API_PID_CONTROL_RH] , Kp, Ki, Kd);
}

void api_pid_init(void)
{

	board_pid_Constructor(&self_.pid[API_PID_CONTROL_RH],
						&self_.input[API_PID_CONTROL_RH],
						&self_.output[API_PID_CONTROL_RH],
						&self_.setpoint[API_PID_CONTROL_RH],
						API_PID_TEMPERATURE_KP, API_PID_TEMPERATURE_KI, API_PID_TEMPERATURE_KD,
						API_PID_TEMPERATURE_PON, API_PID_TEMPERATURE_DIRECTION);

	self_.enable[API_PID_CONTROL_RH]=false;
}
void api_pid_deinit(void)
{

}
void api_pid_loop(void)
{
#if 0
	for(int i =0; i< API_PID_TAG__N; i++){
		if(true == self_.enable[i]){
			board_pid_Compute(&self_.pid[i]);
		}
	}
#endif
}

void api_pid_compute(api_pid_tag_t pid_tag){

	if(true == self_.enable[pid_tag]){
		board_pid_Compute(&self_.pid[pid_tag]);
	}
}


/********************** end of file ******************************************/

