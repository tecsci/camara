/*
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		api_serial.c
* @date		21 jun. 2021
* @author	
*		-Martin Abel Gambarotta   (magambarotta@gmail.com)
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "api_serial.h"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/
uint32_t  api_serial_read(api_uart_id_t uart_id,void* buffer)
{
  return board_serial_read(uart_id ,buffer);
}
void api_serial_write(api_uart_id_t uart_id, void* buffer, uint32_t len)
{
  board_serial_write(uart_id, buffer,len);
}

uint32_t  api_serial_is_available(api_uart_id_t uart_id)
{
  return board_serial_is_available(uart_id);
}

void api_serial_init(){}
void api_serial_deinit(){}
void api_serial_loop(){}

/********************** end of file ******************************************/

/** @}Final Doxygen */
