/*
 * Copyright (c) 2021 Lucas Mancini <mancinilucas95@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @file	api_pid.hpp
 * @date	2021-19-MAY
 * @author:
 *  - Lucas Mancini <mancinilucas95@gmail.com>.
 * @version	v1.0.0
 */

#ifndef _API_PID_HPP_
#define _API_PID_HPP_

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "board.h"

/********************** macros ***********************************************/

/********************** typedef **********************************************/

typedef enum{

	API_PID_CONTROL_RH=0,
	API_PID_TAG__N

}api_pid_tag_t;

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void api_pid_start(api_pid_tag_t pid_tag, double input_o, double setpoint_o);
void api_pid_stop(api_pid_tag_t pid_tag);

double api_pid_get_output(api_pid_tag_t pid_tag);
void api_pid_set_input(api_pid_tag_t pid_tag, double input);
void api_pid_change_setpoint(api_pid_tag_t pid_tag, double setpoint);
double api_pid_get_setpoint(api_pid_tag_t pid_tag);

void api_pid_automatic_mode(api_pid_tag_t pid_tag);
void api_pid_manual_mode(api_pid_tag_t pid_tag);
void api_pid_set_output_limits(api_pid_tag_t pid_tag, double min, double max);

void api_pid_set_tunning_constants(api_pid_tag_t pid_tag, double Kp, double Ki, double Kd);

void api_pid_compute(api_pid_tag_t pid_tag);

void api_pid_init(void);
void api_pid_deinit(void);
void api_pid_loop(void);


#endif /* _API_PID_HPP_ */
/********************** end of file ******************************************/

