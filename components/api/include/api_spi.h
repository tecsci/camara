/* 
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		api_spi.h
* @date		28 jul. 2021
* @author	
*		-Martin Abel Gambarotta   (magambarotta@gmail.com)
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef COMPONENTS_API_INCLUDE_API_SPI_H_
#define COMPONENTS_API_INCLUDE_API_SPI_H_
/********************** inclusions *******************************************/
#include "board_spi.h"
/********************** macros ***********************************************/

/********************** typedef **********************************************/
typedef enum{

	API_SPI_0 = BOARD_SPI_0,
	API_SPI_1 = BOARD_SPI_1,
	API_SPI_2 = BOARD_SPI_2,
	API_SPI_3 = BOARD_SPI_3

}api_spi_id_t;

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void api_spi_read();
void api_spi_write_read(api_spi_id_t id, uint8_t *tx, uint8_t *rx, size_t length);

void api_spi_init();
void api_spi_deinit();
void api_spi_loop();

#endif /* COMPONENTS_API_INCLUDE_API_SPI_H_ */
/** @}Final Doxygen */

