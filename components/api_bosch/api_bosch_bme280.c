/*
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		api_bme280.c
* @date		23 sep. 2021
* @author	
*		-Martin Abel Gambarotta   <magambarotta@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "api_bosch_bme280.h"

#include "esp_log.h"
#include "board_delay.h"
#include "board_i2c.h"

#include "esp_err.h"
#include "driver/i2c.h"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/
static const char *TAG = "api_bme280.c";

/********************** internal data definition *****************************/
static struct {

	struct bme280_dev dev[API_BOSCH_BME280_TAG__N];
	struct bme280_data data[API_BOSCH_BME280_TAG__N];

	uint8_t dev_addr[API_BOSCH_BME280_TAG__N];

	struct bme280_dev devx;
	uint8_t dev_addrx;

} self_;

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static void api_bme280_config_normal_mode_(api_bosch_bme280_tag_t tag){

	uint8_t settings_sel;

	/* Recommended mode of operation: Indoor navigation */
	self_.dev[tag].settings.osr_h = BME280_OVERSAMPLING_1X;
	self_.dev[tag].settings.osr_p = BME280_OVERSAMPLING_16X;
	self_.dev[tag].settings.osr_t = BME280_OVERSAMPLING_2X;
	self_.dev[tag].settings.filter = BME280_FILTER_COEFF_16;
	self_.dev[tag].settings.standby_time = BME280_STANDBY_TIME_62_5_MS;

	settings_sel = BME280_OSR_PRESS_SEL;
	settings_sel |= BME280_OSR_TEMP_SEL;
	settings_sel |= BME280_OSR_HUM_SEL;
	settings_sel |= BME280_STANDBY_SEL;
	settings_sel |= BME280_FILTER_SEL;

	bme280_set_sensor_settings(settings_sel, &(self_.dev[tag]));
	bme280_set_sensor_mode(BME280_NORMAL_MODE, &(self_.dev[tag]));

}


int8_t api_bosch_bme280_i2c_write_(uint8_t reg_addr,uint8_t *reg_data, uint32_t len, void *intf_ptr)
{
	int8_t rslt = 0; /* Return 0 for Success, non-zero for failure */
	uint8_t device_addr = *((uint8_t *) intf_ptr);

	rslt = board_i2c_master_write(BOARD_I2C_0,device_addr,reg_addr,reg_data,len);

	return rslt;
}

int8_t api_bosch_bme280_i2c_read_(uint8_t reg_addr, uint8_t *reg_data, uint32_t len, void *intf_ptr)
{
	int8_t rslt = 0; /* Return 0 for Success, non-zero for failure */
	uint8_t device_addr = *((uint8_t *) intf_ptr);

	rslt = board_i2c_master_read(BOARD_I2C_0,device_addr,reg_addr,reg_data,len);


	return rslt;
}


void api_bosch_bme280_delay_(uint32_t period, void *intf_ptr)
{
	board_delay_micros(period);
}


/********************** external functions definition ************************/
void api_bosch_bme280_print_sensor_data_(struct bme280_data *comp_data){
	ESP_LOGI(TAG,"%0.2f, %0.2f, %0.2f",comp_data->temperature, comp_data->pressure, comp_data->humidity);
}

bool api_bosch_bme280_get_all(api_bosch_bme280_tag_t tag, double* p_temp, double* p_hum, double* p_press){
	if( BME280_OK != bme280_get_sensor_data(BME280_ALL, &(self_.data[tag]), &(self_.dev[tag]))){
		ESP_LOGE(TAG,"ERROR READING BME280");
		return false;
	}
	//api_bosch_bme280_print_sensor_data_(&(self_.data[tag]));
	*p_temp = self_.data[tag].temperature;
	*p_hum = self_.data[tag].humidity;
	*p_press = self_.data[tag].pressure;

	return true;
}

double api_bosch_bme280_get_temperature(api_bosch_bme280_tag_t tag){
	if( BME280_OK != bme280_get_sensor_data(BME280_ALL, &(self_.data[tag]), &(self_.dev[tag]))){
		ESP_LOGE(TAG,"ERROR READING BME280");
		return 0;
	}
	//api_bosch_bme280_print_sensor_data_(&(self_.data[tag]));
	return self_.data[tag].temperature;
}

double api_bosch_bme280_get_pressure(api_bosch_bme280_tag_t tag){
	if( BME280_OK != bme280_get_sensor_data(BME280_ALL, &(self_.data[tag]), &(self_.dev[tag]))){
		ESP_LOGE(TAG,"ERROR READING BME280");
		return 0;
	}
	//api_bosch_bme280_print_sensor_data_(&(self_.data[tag]));
	return self_.data[tag].pressure;
}

double api_bosch_bme280_get_humidity(api_bosch_bme280_tag_t tag){
	if( BME280_OK != bme280_get_sensor_data(BME280_ALL, &(self_.data[tag]), &(self_.dev[tag]))){
		//ESP_LOGE(TAG,"ERROR READING BME280");
		return 0;
	}
	//api_bosch_bme280_print_sensor_data_(&(self_.data[tag]));
	return self_.data[tag].humidity;
}


double api_bosch_bme280_get_temperature_last_measurement(api_bosch_bme280_tag_t tag){
	return self_.data[tag].temperature;
}
double api_bosch_bme280_get_pressure_last_measurement(api_bosch_bme280_tag_t tag){
	return self_.data[tag].pressure;
}
double api_bosch_bme280_get_humidity_last_measurement(api_bosch_bme280_tag_t tag){
	return self_.data[tag].humidity;
}


void api_bosch_bme280_init(){

	int8_t rslt = BME280_OK;
#if 0
	//INIT BME 280
	self_.dev_addrx = BME280_I2C_ADDR_PRIM;


	self_.devx.intf_ptr = &self_.dev_addrx;
	self_.devx.intf = BME280_I2C_INTF;
	self_.devx.read = api_bosch_bme280_i2c_read_;
	self_.devx.write = api_bosch_bme280_i2c_write_;
	self_.devx.delay_us = api_bosch_bme280_delay_;

	rslt = bme280_init(&self_.devx);

	if(rslt != BME280_OK){
		ESP_LOGE(TAG, "Error bme280 1 init %d", rslt);
	}

    api_bme280_config_normal_mode_x();
#endif

#if true == API_BOSCH_BME280_ENABLE_1
	//INIT BME 280 1
	self_.dev_addr[API_BOSCH_BME280_TAG_1] = API_BOSCH_BME280_I2C_ADDR_1;

	self_.dev[API_BOSCH_BME280_TAG_1].chip_id = 1;
	self_.dev[API_BOSCH_BME280_TAG_1].intf_ptr = &(self_.dev_addr[API_BOSCH_BME280_TAG_1]);
	self_.dev[API_BOSCH_BME280_TAG_1].intf = BME280_I2C_INTF;
	self_.dev[API_BOSCH_BME280_TAG_1].read = api_bosch_bme280_i2c_read_;
	self_.dev[API_BOSCH_BME280_TAG_1].write = api_bosch_bme280_i2c_write_;
	self_.dev[API_BOSCH_BME280_TAG_1].delay_us = api_bosch_bme280_delay_;

	rslt = bme280_init(&(self_.dev[API_BOSCH_BME280_TAG_1]));

	if(rslt != BME280_OK){
		ESP_LOGE(TAG, "Error bme280 1 init %d", rslt);
	}
	else{
		ESP_LOGI(TAG, "Succes bme280 1 init %d", rslt);
	}
	 api_bme280_config_normal_mode_(API_BOSCH_BME280_TAG_1);

#endif

#if true == API_BOSCH_BME280_ENABLE_2
	//INIT BME 280 2
	self_.dev_addr[API_BOSCH_BME280_TAG_2] = API_BOSCH_BME280_I2C_ADDR_2;

	self_.dev[API_BOSCH_BME280_TAG_2].chip_id = 2;
	self_.dev[API_BOSCH_BME280_TAG_2].intf_ptr = &(self_.dev_addr[API_BOSCH_BME280_TAG_2]);
	self_.dev[API_BOSCH_BME280_TAG_2].intf = BME280_I2C_INTF;
	self_.dev[API_BOSCH_BME280_TAG_2].read = api_bosch_bme280_i2c_read_;
	self_.dev[API_BOSCH_BME280_TAG_2].write = api_bosch_bme280_i2c_write_;
	self_.dev[API_BOSCH_BME280_TAG_2].delay_us = api_bosch_bme280_delay_;

	rslt = bme280_init(&(self_.dev[API_BOSCH_BME280_TAG_2]));

	if(rslt != BME280_OK){
		ESP_LOGE(TAG, "Error bme280 2 init: %d", rslt);
	}
	else
	{
		 api_bme280_config_normal_mode_(API_BOSCH_BME280_TAG_2);
	}
#endif


}
void api_bosch__bme280_deinit(){

}

void api_bosch_bme280_loop() {

#if true == API_BME280_ENABLE_LOOP_READ

#if true == API_BOSCH_BME280_ENABLE_1
		if( BME280_OK == bme280_get_sensor_data(BME280_ALL, &(self_.data[API_BOSCH_BME280_TAG_1]) , &(self_.dev[API_BOSCH_BME280_TAG_1])) ){
			api_bosch_bme280_print_sensor_data_(&(self_.data[API_BOSCH_BME280_TAG_1]));
		}
		else{
			ESP_LOGE(TAG,"ERROR READING BME280 1");
		}
#endif

#if true == API_BOSCH_BME280_ENABLE_2
		if( BME280_OK == bme280_get_sensor_data(BME280_ALL, &(self_.data[API_BOSCH_BME280_TAG_2]) , &(self_.dev[API_BOSCH_BME280_TAG_2])) ){
			api_bosch_bme280_print_sensor_data_(&(self_.data[API_BOSCH_BME280_TAG_2]));
		}
		else{
			ESP_LOGE(TAG,"ERROR READING BME280 2");
		}
#endif

#endif
}

/********************** end of file ******************************************/

/** @}Final Doxygen */
