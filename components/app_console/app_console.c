/*
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* Copyright (C) <2021>  <Martin Abel Gambarotta   magambarotta@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		app_console.c
* @date		14 jun. 2021
* @author	
*		-Martin Abel Gambarotta   magambarotta@gmail.com
* @version	v1.0.0
* @brief
*
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/


#include "stdio.h"
#include "stdlib.h"
#include "esp_log.h"

#include "api_serial.h"

#include "app_console.h"
#include "mod_console_commands.h"

#include "os.h"



/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/
static const char *TAG = "app_console";
/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/
void app_console_init(void)
{



    xTaskCreate(app_console_loop, "console task" 	// A name just for humans
  			, OS_APP_CONSOLE_TASK_SIZE			// This stack size can be checked & adjusted by reading the Stack Highwater
  			, NULL, OS_APP_CONSOLE_TASK_PRIORITY 		// Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
  			, NULL);


}
void app_console_deinit(void)
{
}
void app_console_loop(void *pvParameters)
{

  ESP_LOGI(TAG,"%s",__func__);
  mod_console_commands_init();

  for(;;) {

  		//Este modo queda bloqueado hasta que llegue alguna interrupcion de lectura o escritura en UART0
      mod_console_commands_loop();

  }
}

/********************** end of file ******************************************/

/** @}Final Doxygen */
