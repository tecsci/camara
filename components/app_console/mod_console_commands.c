/*
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		mod_console_commands.c
* @date		20 jul. 2021
* @author	
*		-Martin Abel Gambarotta   (magambarotta@gmail.com)
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "mod_console_commands.h"
#include "stddef.h"
#include "esp_log.h"
#include "esp_system.h"

#include "esp_console.h"
#include "argtable3/argtable3.h"
#include "linenoise/linenoise.h"
#include "string.h"

#include "api.h"
#include "device.h"
#include "app_control.h"

/********************** macros and definitions *******************************/

#define PROMPT_STR "tecsci_chamber"
#define MAX_ESTATIC_COMMAND 	(8)

/********************** internal data declaration ****************************/
static const char *TAG = "mod_console_commands";
static const char* prompt;

/********************** internal data definition *****************************/

static struct {
    struct arg_int *humidity_target;
    struct arg_end *end;
} camara_args_humidity_t;


static float  target_rh = 40;
/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

#if 0
static int CommandSETCOMMANDCUSTOMAPPHandler(int argc, char **argv) {

	api_program_manager_program_t aux_program;


	int nerrors = arg_parse(argc, argv, (void **) &dip_coating_args_command_custom_t);
	if (nerrors != 0) {
	    arg_print_errors(stderr, dip_coating_args_command_custom_t.end, argv[0]);
	    return 1;
	}

	/*SETCUSTOMPROGRAMAPP  down_vel down_acel down_wait  up_vel up_acel up_wait total_loop  displacemente_sample depth_sample */
	// DOWN LOOP vel acel
	if (dip_coating_args_command_custom_t.down_vel->count){
		aux_program.velocity_down = dip_coating_args_command_custom_t.down_vel->ival[0];
	}
	if(dip_coating_args_command_custom_t.down_acel->count){
		aux_program.acceleration_down = dip_coating_args_command_custom_t.down_acel->ival[0];
	}
	//WAIT DOWN
	if(dip_coating_args_command_custom_t.down_wait->count){
		aux_program.wait_down = 	dip_coating_args_command_custom_t.down_wait->ival[0] ;
	}
	//UP LOOP vel acel
	if(dip_coating_args_command_custom_t.up_vel->count){
		aux_program.velocity_up = 	dip_coating_args_command_custom_t.up_vel->ival[0];
	}
	if(dip_coating_args_command_custom_t.up_acel->count){
		aux_program.acceleration_up =	dip_coating_args_command_custom_t.up_acel->ival[0];
	}
	//WAIT UP
	if(dip_coating_args_command_custom_t.up_wait->count){
		aux_program.wait_up = dip_coating_args_command_custom_t.up_wait->ival[0];
	}
	//NUMBER OF LOOP
	if(dip_coating_args_command_custom_t.total_loop->count){
		aux_program.loops = dip_coating_args_command_custom_t.total_loop->ival[0];
	}

	//TODO Fix:  recibir cadena completa de comandos.
#if 0

	//Desplazamiento hasta muestra
	if(dip_coating_args_command_custom_t.displacement_to_sample->count){
		aux_program.displacement_to_sample = (dip_coating_args_command_custom_t.displacement_to_sample->ival[0]);    // Expresado en mm
	}
	//Profundidad DELTADIP
	if(dip_coating_args_command_custom_t.depth_sample->count){
		aux_program.depth_sample = ( dip_coating_args_command_custom_t.depth_sample->ival[0]);
	}
#endif

	aux_program.displacement_to_sample=100; //mm
	aux_program.depth_sample=20;//mm



	if (xQueueSend(xQueueCompleteProgram, &aux_program, (TickType_t ) 0) != pdPASS)
	  {
	    ESP_LOGE(TAG, "Comando descartado, fallo al enviar mensaje por cola");
	  }
	else
	  ESP_LOGI(TAG, "Datos Cargados Set Commands App!");

	return 0;
}



static int CommandDELTADIPHandler(int argc, char **argv) {

processCommand_t aux_process_comand;
aux_process_comand.commandnumber = PROCESS_COMMAND_DELTADIP;

int nerrors = arg_parse(argc, argv, (void **) &dip_coating_args_depth_sample_t);
if (nerrors != 0) {
    arg_print_errors(stderr, dip_coating_args_depth_sample_t.end, argv[0]);
    return 1;
}

  if (dip_coating_args_depth_sample_t.depth_sample->count && (processDipCoating.config.status == 0)) {
  		//Paso el valor en mm
      processDipCoating.config.displacement_delta_sample = (uint32_t) dip_coating_args_depth_sample_t.depth_sample->ival[0] / 0.00007851;
      if (xQueueSend(xQueueIndividualCommand, &aux_process_comand,(TickType_t) 0) != pdPASS) {
	  ESP_LOGE(TAG,"Comando descartado, fallo al enviar mensaje por cola");
      }
      ESP_LOGI(TAG, "Comando DELTADIP recibido");
      return 0;
} else
  ESP_LOGE(TAG,"Ingrese 2 argumentos -> DELTADIP X( Profundidad de Inmersion de la muenstra en mm)");
  return 1;
}

/*Handler to found Cero Machine Target */
static int CommandCEROMACHINEHandler (int argc, char **argv) {
	processCommand_t aux_process_comand;
	aux_process_comand.commandnumber = PROCESS_COMMAND_CERO_MACHINE;

	if (processDipCoating.config.status == 0) {
		if (xQueueSend(xQueueIndividualCommand, &aux_process_comand,(TickType_t ) 0) != pdPASS) {
			ESP_LOGE(TAG, "Comando descartado, fallo al enviar mensaje por cola");
		}
		ESP_LOGI(TAG, "Comando CERO_MACHINE recibido");
		return 0;
	}
	else {
		ESP_LOGE(TAG, "Comando CERO_MACHINE descartado, maquina trabajando");
		return 1;
	}

}

/*RUN PROCESS HANDLER*/
/*Handler to RUn the LOADED process,
 * turn on the RUN flag to RUN*/
static int CommandRUNHandler(int argc, char **argv) {

	processCommand_t aux_process_comand;
	aux_process_comand.commandnumber = PROCESS_COMMAND_RUN;

	if (processDipCoating.config.status == 0 && processDipCoating.command != NULL) {

		if (xQueueSend(xQueueIndividualCommand, &aux_process_comand, (TickType_t) 0) != pdPASS) {
			ESP_LOGE(TAG, "Comando descartado, fallo al enviar mensaje por cola");
		}
		ESP_LOGI(TAG, "Comando RUN recibido");
		return 0;
	} else {
		ESP_LOGE(TAG,"Comando RUN descartado, maquina trabajando");
		return 1;
	}
}


/*SINGLE MOVEMENT HANDLERS*/
/*this tiny handlers only call the corresponding process function*/
static int CommandUPFASTHandler(int argc, char **argv) {

	processCommand_t aux_process_comand;
	aux_process_comand.commandnumber = PROCESS_COMMAND_UPFAST;

	if (processDipCoating.config.status == 0) {

		if (xQueueSend(xQueueIndividualCommand, &aux_process_comand,
				(TickType_t) 0) != pdPASS) {
			// Failed to post the message, even after 10 ticks.
			ESP_LOGE(TAG,
					"Comando descartado, fallo al enviar mensaje por cola");
		}
		ESP_LOGI(TAG, "Comando UPFAST recibido");
		return 0;
	} else
		ESP_LOGE(TAG,"Comando UPFAST descartado, maquina trabajando");
		return 1;
}



static int CommandUPHandler(int argc, char **argv) {

	processCommand_t aux_process_comand;
	aux_process_comand.commandnumber = PROCESS_COMMAND_UP;

	if (processDipCoating.config.status == 0) {
		//modQueue_Write(&queueconsolareception, &aux_process_comand);
		if (xQueueSend(xQueueIndividualCommand, &aux_process_comand,
				(TickType_t) 0) != pdPASS) {
			// Failed to post the message, even after 10 ticks.
			ESP_LOGE(TAG,
					"Comando descartado, fallo al enviar mensaje por cola");
		}
		ESP_LOGI(TAG, "Comando UP recibido");
		return 0;
	} else
		ESP_LOGE(TAG,
				"Comando UP descartado, maquina trabajando");
	return 1;

}

static int CommandUPSLOWHandler(int argc, char **argv) {

	processCommand_t aux_process_comand;
	aux_process_comand.commandnumber = PROCESS_COMMAND_UPSLOW;

	if (processDipCoating.config.status == 0) {
		//modQueue_Write(&queueconsolareception, &aux_process_comand);
		if (xQueueSend(xQueueIndividualCommand, &aux_process_comand,
				(TickType_t) 0) != pdPASS) {
			// Failed to post the message, even after 10 ticks.
			ESP_LOGE(TAG,
					"Comando descartado, fallo al enviar mensaje por cola");
		}
		ESP_LOGI(TAG, "Comando UPSLOW recibido");
		return 0;
	} else
		ESP_LOGE(TAG,
				"Comando UPSLOW descartado, maquina trabajando");
	return 1;
}

static int CommandDOWNFASTHandler(int argc, char **argv) {
	processCommand_t aux_process_comand;


	aux_process_comand.commandnumber = PROCESS_COMMAND_DOWNFAST;

	if (processDipCoating.config.status == 0) {

		if (xQueueSend(xQueueIndividualCommand, &aux_process_comand,
				(TickType_t) 0) != pdPASS) {
			// Failed to post the message, even after 10 ticks.
			ESP_LOGE(TAG,
					"Comando descartado, fallo al enviar mensaje por cola");
		}
		ESP_LOGI(TAG, "Comando DOWNFAST recibido");
		return 0;
	} else
		ESP_LOGE(TAG,
				"Comando DOWNFAST descartado, maquina trabajando");
	return 1;
}

static int CommandDOWNHandler(int argc, char **argv) {
	processCommand_t aux_process_comand;
	aux_process_comand.commandnumber = PROCESS_COMMAND_DOWN;

	if (processDipCoating.config.status == 0) {

		if (xQueueSend(xQueueIndividualCommand, &aux_process_comand,
				(TickType_t) 0) != pdPASS) {
			// Failed to post the message, even after 10 ticks.
			ESP_LOGE(TAG,
					"Comando descartado, fallo al enviar mensaje por cola");
		}
		ESP_LOGI(TAG, "Comando DOWN recibido");
		return 0;
	} else
		ESP_LOGE(TAG,
				"Comando DOWN descartado, maquina trabajando");
	return 1;
}

static int CommandDOWNSLOWHandler (int argc, char **argv) {

#if 1

	processCommand_t aux_process_comand;
	aux_process_comand.commandnumber = PROCESS_COMMAND_DOWNSLOW;

	if (processDipCoating.config.status == 0) {
		if (xQueueSend(xQueueIndividualCommand, &aux_process_comand, (TickType_t) 0) != pdPASS) {
			ESP_LOGE(TAG, "Comando descartado, fallo al enviar mensaje por cola");
		}
		ESP_LOGI(TAG, "Comando DOWNSLOW recibido");
		return 0;
	}
	else
		ESP_LOGE(TAG, "Comando DOWNSLOW descartado, maquina trabajando");
	return 1;

#endif



#if 0

	dip_program_t dip_,aux_dip;
	process_t aux_processDipCoating;


	//Charge Custom process
	aux_processDipCoating.command= cmdProcessCustom;
	aux_processDipCoating.commandlen=0;
	aux_processDipCoating.state.commandIndex=MAX_ESTATIC_COMMAND;
	aux_processDipCoating.state.commandActualIndex=0;
	aux_processDipCoating.state.flags=0;
	aux_processDipCoating.config.status=0;
	aux_processDipCoating.run_protocol =false;


	/*Cargo programa en memoria*/

	strcpy(dip_.name,"Hector Dipping");
	aux_dip.velocity_down=100;
	aux_dip.acceleration_down=10000;
	aux_dip.wait_down= 2;

	aux_dip.velocity_up= 300;
	aux_dip.acceleration_up=10000;
	aux_dip.wait_up= 2;

	aux_dip.displacement_delta_sample=20;
	aux_dip.displacement_to_sample=250;
	aux_dip.loops=0;

//	api_program_manager_write(&dip_, 3);
//	ESP_LOGI(TAG, "velocity_down:%d",	dip_.velocity_down);
//	ESP_LOGI(TAG, "loops:%d",	dip_.loops);
//	api_program_manager_read(&aux_dip, 3);


	aux_processDipCoating.command[2].argument.spin.velocity = 			(aux_dip.velocity_down 	 * 212.2719735);
	aux_processDipCoating.command[2].argument.spin.acceleration = 	32000; //(aux_dip.acceleration_down * 3.537866);
	//WAIT DOWN
	aux_processDipCoating.command[3].argument.wait.time = 	 				aux_dip.wait_down ;
	//UP LOOP vel acel
	aux_processDipCoating.command[4].argument.spin.velocity = 			(aux_dip.velocity_up * 212.2719735);
	aux_processDipCoating.command[4].argument.spin.acceleration =		32000; //(aux_dip.acceleration_up * 3.537866);
	//WAIT UP
	aux_processDipCoating.command[5].argument.wait.time = 					aux_dip.wait_up;
	//NUMBER OF LOOP
	aux_processDipCoating.command[6].argument.spin.velocity = 			aux_dip.loops;

	aux_processDipCoating.config.displacement_to_sample = 					(	aux_dip.displacement_to_sample * 12737);    // Expresado en mm
	//Profundidad DELTADIP
	aux_processDipCoating.config.displacement_delta_sample = 				(	aux_dip.displacement_delta_sample / K_DEZPLAZAMIENTO_LINEAL);

  ESP_LOGI(TAG,"Envio Complete Program");
	if (xQueueSend(xQueueCompleteProgram, &aux_processDipCoating, (TickType_t ) 0) != pdPASS)
	  {
	    ESP_LOGE(TAG, "Comando descartado, fallo al enviar mensaje por cola");
	  }
	else
	  ESP_LOGI(TAG, "Datos Cargados!");

#endif

	return 0;
}

//TODO sacar este STOP de aca!!!

//VER ESTE COMANDO EN PARTICULAR
static int  CommandSTOPHandler(int argc, char **argv) {

	int32_t lectura, lecturaBanderas;

	Evalboards.ch1.enableDriver(DRIVER_DISABLE);
	processDipCoating.run_protocol= false;
	processDipCoating.config.status = 0;


	Evalboards.ch1.readRegister(0, 0x21, &lectura);

	Evalboards.ch1.writeRegister(0, 0x21, lectura);
	Evalboards.ch1.writeRegister(0, 0x2D, lectura);

//TODO 	ver este tema de banderas
	Evalboards.ch1.readRegister(0, 0x35, &lecturaBanderas);

//	Evalboards.ch1.writeRegister(0, 0x21, lectura);
//	Evalboards.ch1.writeRegister(0, 0x2D, lectura);
//	Evalboards.ch1.readRegister(0, 0x35, &lecturaBanderas);

	ESP_LOGI(TAG, "Comando STOP recibido");

	return 0;

}

static int  CommandREADDATAHandler(int argc, char **argv) {

	processCommand_t aux_process_comand;
	aux_process_comand.commandnumber = PROCESS_COMMAND_READDATA;

	if (processDipCoating.config.status == 0) {

		if (xQueueSend(xQueueIndividualCommand, &aux_process_comand,
				(TickType_t) 0) != pdPASS) {
			ESP_LOGE(TAG,
					"Comando descartado, fallo al enviar mensaje por cola");
		}
		ESP_LOGI(TAG, "Comando READDATA recibido");
		return 0;
	} else
		ESP_LOGE(TAG,"Comando READDATA descartado, maquina trabajando");
	return 1;
}

static int  CommandENA_DRIVERHandler(int argc, char **argv) {

	processCommand_t aux_process_comand;
	aux_process_comand.commandnumber = PROCESS_COMMAND_ENA_DRIVER;

	if (processDipCoating.config.status == 0) {
		if (xQueueSend(xQueueIndividualCommand, &aux_process_comand,
				(TickType_t) 0) != pdPASS) {

			ESP_LOGE(TAG,
					"Comando descartado, fallo al enviar mensaje por cola");
		}
		ESP_LOGI(TAG, "Comando ENA_DRIVER recibido");
		return 0;
	} else
		ESP_LOGE(TAG,
				"Comando ENA_DRIVER descartado, maquina trabajando");
	return 1;
}

static int  CommandDIS_DRIVERHandler(int argc, char **argv) {

	processCommand_t aux_process_comand;
	aux_process_comand.commandnumber = PROCESS_COMMAND_DIS_DRIVER;

	Evalboards.ch1.enableDriver(DRIVER_DISABLE);

	if (processDipCoating.config.status == 0) {

		if (xQueueSend(xQueueIndividualCommand, &aux_process_comand,
				(TickType_t) 0) != pdPASS) {
			ESP_LOGE(TAG,
					"Comando descartado, fallo al enviar mensaje por cola");
		}
		ESP_LOGI(TAG, "Comando DIS_DRIVER recibido");
		return 0;
	} else
		ESP_LOGE(TAG,
				"Comando DIS_DRIVER descartado, maquina trabajando");
	return 1;
}

static int  CommandRESETHandler(int argc, char **argv) {

	processCommand_t aux_process_comand;
	aux_process_comand.commandnumber = PROCESS_COMMAND_RESET;

	Evalboards.ch1.enableDriver(DRIVER_DISABLE);

	if (xQueueSend(xQueueIndividualCommand, &aux_process_comand, (TickType_t) 0) != pdPASS) {
		ESP_LOGE(TAG,
				"Comando descartado, fallo al enviar mensaje por cola");
	}
	ESP_LOGI(TAG, "Comando RESET recibido");
	return 0;

}


static int  CommandCERO_SAMPLEHandler(int argc, char **argv) {

	processCommand_t aux_process_comand;
	aux_process_comand.commandnumber = PROCESS_COMMAND_CERO_SAMPLE;


	if (xQueueSend(xQueueIndividualCommand, &aux_process_comand,(TickType_t) 0) != pdPASS) {
	    		ESP_LOGE(TAG,"Comando descartado, fallo al enviar mensaje por cola");
	    }
	    		ESP_LOGI(TAG, "Comando CERO_SAMPLE recibido");
	return 0;

}
#endif

#if 0
static int  CommandDOWN1Handler(int argc, char **argv) {
	app_sramp_single_down(1);
	return 0;
}
#endif

static int  CommandTIMEHandler(int argc, char **argv) {
	uint32_t time_milis=esp_log_timestamp();
	ESP_LOGI(TAG, "Time_Milis: -> %u",time_milis); 			//Tiempo en milis desde inicio
	return 0;
}



static int CommandHUMIHandler(int argc, char **argv) {

	bool dummy_send = true;

	int nerrors = arg_parse (argc, argv, (void**) &camara_args_humidity_t);
	if (nerrors != 0) {
		arg_print_errors (stderr, camara_args_humidity_t.end, argv[0]);
		return 1;
	}


	//ESP_LOGI(TAG, "mod_console:%d",camara_args_humidity_t.humidity_target->ival[0]);

	printf("chamber_console_targetrh %d \n",camara_args_humidity_t.humidity_target->ival[0]);

	target_rh = camara_args_humidity_t.humidity_target->ival[0];

	xQueueGenericSend(app_control_stop_queue, &dummy_send,0U, 0U);
	vTaskDelay(100/ portTICK_PERIOD_MS);
	xQueueGenericSend(app_control_params_queue,&target_rh,0U, 0U);

	return 0;
}




#if 0
static int CommandReadRegisterHandler (int argc, char **argv) {

	int rx_register;

	int nerrors = arg_parse (argc, argv, (void**) &dip_coating_args_tmc_register_t);
	if (nerrors != 0) {
		arg_print_errors (stderr, dip_coating_args_tmc_register_t.end, argv[0]);
		return 1;
	}

	Evalboards.ch1.readRegister (0, dip_coating_args_tmc_register_t.tmc_register_address->ival[0], &rx_register);
	ESP_LOGI(TAG,"Address->0x%x\tValue->%#010x",(uint8_t)dip_coating_args_tmc_register_t.tmc_register_address->ival[0], rx_register);

	return 0;
}


static int CommandWriteRegisterHandler (int argc, char **argv) {

	int nerrors = arg_parse (argc, argv, (void**) &dip_coating_args_tmc_register_t);
	if (nerrors != 0) {
		arg_print_errors (stderr, dip_coating_args_tmc_register_t.end, argv[0]);
		return 1;
	}

	Evalboards.ch1.writeRegister (0, dip_coating_args_tmc_register_t.tmc_register_address->ival[0], dip_coating_args_tmc_register_t.tmc_register_value->ival[0]);
	ESP_LOGI(TAG,"Address->0x%x\tValue->%d" ,(uint8_t) dip_coating_args_tmc_register_t.tmc_register_address->ival[0],  dip_coating_args_tmc_register_t.tmc_register_value->ival[0]);


	return 0;
}
#endif


#if 0
static int CommandLOADPROGRAMCUSTOMHandler(int argc, char **argv){
	printf("Hola Hector\r\n");
	//ProcessLoadProgramCustom(&processDipCoating);

	int nerrors = arg_parse(argc, argv, (void **) &dip_coating_args_t);
	if (nerrors != 0) {
	    arg_print_errors(stderr, dip_coating_args_t.end, argv[0]);
	    return 1;
	}

	int valor_1 = dip_coating_args_t.value_1->ival[0];
	int valor_2 = dip_coating_args_t.value_2->ival[0];
	int valor_3 = dip_coating_args_t.value_3->ival[0];
	printf("Guardo: %d %d %d \r\n",valor_1,valor_2,valor_3);
	return 0;
}
#endif



 static void mod_console_register_commands_(){

#if 0
   dip_coating_args_command_custom_t.down_vel  								=	arg_int0(NULL,"dv","<n>", "Down Velocity");
   dip_coating_args_command_custom_t.down_acel  							=	arg_int0(NULL,"da","<n>", "Down Acceleration");
   dip_coating_args_command_custom_t.down_wait  							=	arg_int0(NULL,"dw","<n>", "Down Wait ,ms");
   dip_coating_args_command_custom_t.up_vel  									=	arg_int0(NULL,"uv","<n>", "Up Velocity");
   dip_coating_args_command_custom_t.up_acel  								=	arg_int0(NULL,"ua","<n>", "Up Acceleration");
   dip_coating_args_command_custom_t.up_wait  								=	arg_int0(NULL,"uw","<n>", "Up Wait, ms");
   dip_coating_args_command_custom_t.total_loop  							=	arg_int0(NULL,"ln","<n>", "Total Loop");
//   dip_coating_args_command_custom_t.displacement_to_sample 	= arg_int0(NULL,"ds","<n>", "Displacement to sample, mm");
//   dip_coating_args_command_custom_t.depth_sample 						=	arg_int0(NULL,"dip","<n>","Depth sample");
   dip_coating_args_command_custom_t.end 											= arg_end (7);
#endif


  const esp_console_cmd_t timestamp = {
      .command = "timestamp",
      .help = "System Time",
      .hint = NULL,
      .func = &CommandTIMEHandler,
  };
  ESP_ERROR_CHECK( esp_console_cmd_register(&timestamp) );


  camara_args_humidity_t.humidity_target = arg_int0("h",NULL ,"<n>","Humidity Target");
  camara_args_humidity_t.end = arg_end(1);

  const esp_console_cmd_t h_target = {
      .command = "htg",
      .help = "Humidity Target",
      .hint = NULL,
      .func = &CommandHUMIHandler,
	  .argtable = &camara_args_humidity_t,
  };
  ESP_ERROR_CHECK( esp_console_cmd_register(&h_target) );

 }



void mod_console_commands_init()
{
    /* Initialize the console */
    esp_console_config_t console_config = {
            .max_cmdline_args = 12,
            .max_cmdline_length = 512,
#if CONFIG_LOG_COLORS
            .hint_color = atoi(LOG_COLOR_BLUE)
#endif
    };
    ESP_ERROR_CHECK( esp_console_init(&console_config) );

    /* Configure linenoise line completion library */
    /* Enable multiline editing. If not set, long commands will scroll within
     * single line.
     */
    linenoiseSetMultiLine(1);

    /* Tell linenoise where to get command completions and hints */
    linenoiseSetCompletionCallback(&esp_console_get_completion);
    linenoiseSetHintsCallback((linenoiseHintsCallback*) &esp_console_get_hint);
    /* Set command history size */
    linenoiseHistorySetMaxLen(20);
    /* Don't return empty lines */
    linenoiseAllowEmpty(false);


    /* Register commands */
    esp_console_register_help_command();

    /* Dip Coater Register Commands*/
    mod_console_register_commands_();


    /* Prompt to be printed before each line.
     * This can be customized, made dynamic, etc.
     */
    prompt = LOG_COLOR_I PROMPT_STR "> " LOG_RESET_COLOR;

    //"Press Enter or Ctrl+C will terminate the console environment.\n");
    printf("\n"
           "TECSCI_SAS	ESP-IDF Console.\n"
           "Type 'help' to get the list of commands.\n"
           "Use UP/DOWN arrows to navigate through command history.\n"
           "Press TAB when typing command name to auto-complete.\n"
           "Press Ctrl+C will terminate the console environment.\n");

//    /* Figure out if the terminal supports escape sequences */
    int probe_status = linenoiseProbe();
    if (probe_status) { /* zero indicates success */
        printf("\n"
               "Your terminal application does not support escape sequences.\n"
               "Line editing and history features are disabled.\n"
               "On Windows, try using Putty instead.\n");
        linenoiseSetDumbMode(1);

        /* Since the terminal doesn't support escape sequences,
         * don't use color codes in the prompt.
         */
      prompt = PROMPT_STR "> ";

    }

}

void mod_console_commands_deinit()
{

}
void mod_console_commands_loop()
{
  /* Main loop */
      /* Get a line using linenoise.
       * The line is returned when ENTER is pressed.
       */
      char* line = linenoise(prompt);
      if (line == NULL) { /* Break on EOF or error */

      		//ESP_LOGE(TAG, "Error or end-of-input, terminating console");
      		//esp_console_deinit();
      		//return;
      	linenoiseFree(line);
      	return;

      }
      /* Add the command to the history if not empty*/
      if (strlen(line) > 0) {
          linenoiseHistoryAdd(line);
      }

      /* Try to run the command */
      int ret;
      esp_err_t err = esp_console_run(line, &ret);

      if (err == ESP_ERR_NOT_FOUND) {
          printf("Unrecognized command\n");
      } else if (err == ESP_ERR_INVALID_ARG) {
          // command was empty
      } else if (err == ESP_OK && ret != ESP_OK) {
          printf("Command returned non-zero error code: 0x%x (%s)\n", ret, esp_err_to_name(ret));
      } else if (err != ESP_OK) {
          printf("Internal error: %s\n", esp_err_to_name(err));
      }

      /* linenoise allocates line buffer on the heap, so need to free it */
      linenoiseFree(line);

}

/********************** end of file ******************************************/

/** @}Final Doxygen */
