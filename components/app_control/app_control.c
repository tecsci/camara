/*
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		app_control.c
* @date		7 Ap 2022
* @author	
*		-Lucas Mancini
* @version	v1.0.0
* @brief
*
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "app_control.h"

#include "api.h"
#include "api_bosch_bme280.h"
#include "os.h"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/
static const char *TAG = "app_control";

/********************** internal data definition *****************************/

static struct{

	app_control_params_t params;
	app_control_status_t status;

	float pid_output;

	double actual_rh;
	double ambient_rh;
	bool dummy;
	int dry_ena;
	int hum_ena;


}self_;

#if true == APP_CONTROL_EXPERIMENT_1_ENABLE

static struct{

	uint32_t id;

	uint32_t aux_test_counter;

	bool actual_dryer_state;
	bool actual_humidifier_state;

	uint32_t dryer_on_counter;
	uint32_t humidifier_on_counter;

	uint32_t dryer_off_counter;
	uint32_t humidifier_off_counter;

}self_testing_;
#endif


/********************** external data definition *****************************/

QueueHandle_t app_control_params_queue = NULL;

QueueHandle_t app_control_stop_queue = NULL;

/********************** internal functions definition ************************/

static double get_ambient_rh_(void){

	return  api_bosch_bme280_get_humidity(API_BOSCH_BME280_TAG_2);
}

static double get_actual_rh_(void){

	return api_bosch_bme280_get_humidity(API_BOSCH_BME280_TAG_1);
}

static void humidifier_enable_(bool enable ){

	if((self_.params.target_rh >= self_.ambient_rh) || (self_.params.target_rh- self_.actual_rh > APP_CONTROL_HUMIFIER_ENABLE_RANGE)){

		board_gpio_write(APP_CONTROL_HUMIFIER_PIN, enable);
		self_.hum_ena=1;
	}
	else{
		board_gpio_write(APP_CONTROL_HUMIFIER_PIN, false);
		self_.hum_ena=0;
	}


}

static void dryer_enable_(bool enable){

	if((self_.params.target_rh < self_.ambient_rh) || ( self_.actual_rh - self_.params.target_rh > APP_CONTROL_DRYER_ENABLE_RANGE)){

		board_gpio_write(APP_CONTROL_DRYER_PIN, enable);
		self_.dry_ena=1;
	}
	else{
		board_gpio_write(APP_CONTROL_DRYER_PIN, false);
		self_.dry_ena=0;
	}


}

/********************** external functions definition ************************/
void app_control_init(void)
{
	xTaskCreate(app_control_loop, "control task" 	// A name just for humans
			, OS_APP_CONTROL_TASK_SIZE 			// This stack size can be checked & adjusted by reading the Stack Highwater
			, NULL, OS_APP_CONTROL_TASK_PRIORITY 		// Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
			, NULL);


}
void app_control_deinit(void)
{
}
void app_control_loop(void *pvParameters)
{
	ESP_LOGI(TAG,"%s",__func__);

#if true == APP_CONTROL_EXPERIMENT_1_ENABLE
	self_testing_.id =0;
	self_testing_.aux_test_counter=0;
	self_testing_.dryer_on_counter=0;
	self_testing_.humidifier_on_counter=0;
	self_testing_.actual_dryer_state=false;
	self_testing_.actual_humidifier_state=false;
#endif

	//INIT STATUS OFF
    self_.status=APP_CONTROL_CHAMBER_OFF;

    //CREATE PARAMS QUEUE
	app_control_params_queue = xQueueCreate( OS_APP_CONTROL_PARAMS_QUEUE_SIZE , sizeof( app_control_params_t) );
	if( ( app_control_params_queue == NULL ) ){
		for(;;){
			  ESP_LOGE(TAG, "mod_hmi_RX_task_loop: error creating app_control_params_queue");
			vTaskDelay(1000/ portTICK_PERIOD_MS);
		}
	}

    //CREATE STOP QUEUE
	app_control_stop_queue = xQueueCreate( OS_APP_CONTROL_STOP_QUEUE_SIZE , sizeof( bool) );
	if( ( app_control_stop_queue == NULL ) ){
		for(;;){
			  ESP_LOGE(TAG, "mod_hmi_RX_task_loop: error creating app_control_params_queue");
			vTaskDelay(1000/ portTICK_PERIOD_MS);
		}
	}


	for(;;) {
#if 0
		 self_.actual_rh=get_actual_rh_();
		 ESP_LOGI(TAG,"actual RH: %f",self_.actual_rh );
		      vTaskDelay(OS_APP_CONTROL_TASK_PERIOD/ portTICK_PERIOD_MS);
#endif

	     if (xQueueReceive( app_control_params_queue, &self_.params, portMAX_DELAY) == pdTRUE ){

#if true == APP_CONTROL_EXPERIMENT_1_ENABLE
	    	 printf("chamber_status START,%f \n", self_.params.target_rh);
#else
	    	 ESP_LOGI(TAG,"app_control_params_queue received-  target rh: %f",self_.params.target_rh );
#endif

			 self_.status=APP_CONTROL_CHAMBER_BALANCED;

	    	 if(true == APP_CONTROL_USE_DEFAULT_RH_GAPS){
	    		 self_.params.upper_norm_gap_rh =APP_CONTROL_UPPER_NORM_GAP_RH_DEFAULT;
	    		 self_.params.lower_norm_gap_rh =APP_CONTROL_LOWER_NORM_GAP_RH_DEFAULT;
	    		 self_.params.upper_factor_rh =APP_CONTROL_UPPER_FACTOR_RH_DEFAULT;
	    		 self_.params.lower_factor_rh =APP_CONTROL_LOWER_FACTOR_RH_DEFAULT;
	    		 self_.dry_ena=0;
	    		 self_.hum_ena=0;
	    	 }
#if 0 // DEPRECATED --> FOR PID
			 	self_.actual_rh=get_actual_rh_();
				api_pid_start(API_PID_CONTROL_RH, self_.actual_rh,  self_.params.target_rh);
				api_pid_automatic_mode(API_PID_CONTROL_RH);
				api_pid_set_output_limits(API_PID_CONTROL_RH, 0, 1);
				api_pid_change_setpoint(API_PID_CONTROL_RH, self_.params.target_rh);
#endif

	    	 if(NULL != app_control_stop_queue){
	 	    	while(xQueueReceive( app_control_stop_queue, &self_.dummy, 0) != pdTRUE ){

	 				 vTaskDelay(OS_APP_CONTROL_TASK_PERIOD/portTICK_PERIOD_MS);


	 				 self_.actual_rh=get_actual_rh_();
//	 				printf("chamber_actual_rh %f \n",self_.actual_rh );
	 				 self_.ambient_rh=get_ambient_rh_();
//	 				printf("chamber_ambient_rh %f \n",self_.ambient_rh );

	 				 if(APP_CONTROL_CHAMBER_BALANCED == self_.status){

	 					 //ACTUAL > (TARGET + (NORM_UPPER_GAP - TARGET * FACTOR))
	 					 if(self_.actual_rh>(self_.params.target_rh+(self_.params.upper_norm_gap_rh - (self_.params.target_rh * (self_.params.upper_factor_rh))))){
	 	 					self_.status=APP_CONTROL_CHAMBER_DRYINGUP;
	 	 					humidifier_enable_(false);
	 	 					dryer_enable_(true);
	 					 }

	 					 //ACTUAL < (TARGET - (NORM_LOWER_GAP - TARGET * FACTOR))
	 					 if(self_.actual_rh < (self_.params.target_rh-(self_.params.lower_norm_gap_rh - (self_.params.target_rh * (self_.params.lower_factor_rh))))){
	 	 					self_.status=APP_CONTROL_CHAMBER_HUMIDIFYING;
	 	 					humidifier_enable_(true);
	 	 					dryer_enable_(false);
	 					 }

	 				 }
	 				 else if(APP_CONTROL_CHAMBER_DRYINGUP == self_.status){

	 					 if(self_.actual_rh< (self_.params.target_rh)){
	 	 					self_.status=APP_CONTROL_CHAMBER_BALANCED;
	 	 					humidifier_enable_(false);
	 	 					dryer_enable_(false);
	 					 }


	 				 }
	 				 else if(APP_CONTROL_CHAMBER_HUMIDIFYING == self_.status){

	 					 if(self_.actual_rh > (self_.params.target_rh)){
		 	 					humidifier_enable_(false);
		 	 					dryer_enable_(false);
	 	 					self_.status=APP_CONTROL_CHAMBER_BALANCED;
	 					 }

#if 0
	 					 if (self_.actual_rh < self_.ambient_rh){
		 					 if(self_.actual_rh > (self_.params.target_rh - 1 )){
		 	 					humidifier_enable_(false);
		 	 					dryer_enable_(false);
		 					 }
	 					 }
#endif
	 				 }
#if 0 // DEPRECATED --> FOR PID
					//INPUT
					api_pid_set_input(API_PID_CONTROL_RH, self_.actual_rh);
					//COMPUTE PID
					api_pid_compute(API_PID_CONTROL_RH);
					self_.pid_output = api_pid_get_output(API_PID_CONTROL_RH);
					//OUTPUT
					if(self_.pid_output > 0.3){
 	 					humidifier_enable_(true);	dryer_enable_(false);
 	 					self_.status=APP_CONTROL_CHAMBER_HUMIDIFYING;
					}
					else if(self_.pid_output < 0.3){
 	 					humidifier_enable_(false);	dryer_enable_(true);
 	 					self_.status=APP_CONTROL_CHAMBER_DRYINGUP;
					}
					else{
 	 					humidifier_enable_(false); dryer_enable_(false);
 	 					 self_.status=APP_CONTROL_CHAMBER_BALANCED;
					}
#endif
#if 0  //DEPRECATED --> ALGORITMO CON SUBGAPS Y SIN FACTOR
	 				 if(APP_CONTROL_CHAMBER_BALANCED == self_.status){
	 					 //ACTUAL > TARGET + GAP
	 					 if(self_.actual_rh>(self_.params.target_rh+self_.params.upper_gap_rh)){
	 	 					self_.status=APP_CONTROL_CHAMBER_DRYINGUP;
	 	 					humidifier_enable_(false);
	 	 					dryer_enable_(true);
	 					 }
	 					 //ACTUAL < TARGET - GAP
	 					 if(self_.actual_rh<(self_.params.target_rh-self_.params.lower_gap_rh)){
	 	 					self_.status=APP_CONTROL_CHAMBER_HUMIDIFYING;
	 	 					humidifier_enable_(true);
	 	 					dryer_enable_(false);
	 					 }
	 				 }
	 				 else if(APP_CONTROL_CHAMBER_DRYINGUP == self_.status){
	 					 if(self_.actual_rh< (self_.params.target_rh-self_.params.lower_subgap_rh )){
	 	 					self_.status=APP_CONTROL_CHAMBER_BALANCED;
	 	 					humidifier_enable_(false);
	 	 					dryer_enable_(false);
	 					 }
	 				 }
	 				 else if(APP_CONTROL_CHAMBER_HUMIDIFYING == self_.status){
	 					 if(self_.actual_rh > (self_.params.target_rh+self_.params.upper_subgap_rh )){
	 	 					self_.status=APP_CONTROL_CHAMBER_BALANCED;
	 	 					humidifier_enable_(false);
	 	 					dryer_enable_(false);
	 					 }
	 				 }
#endif
#if 0 //ALGORITMO SOLO CON LIMITES
	 				 //BALANCED
	 				if(self_.actual_rh<=(self_.params.target_rh+self_.params.upper_gap_rh)
	 						&& self_.actual_rh>=(self_.params.target_rh-self_.params.lower_gap_rh)){
	 					ESP_LOGI(TAG,"State: keep Humidity");
	 					self_.status=APP_CONTROL_CHAMBER_BALANCED;
	 					humidifier_enable_(false);
	 					dryer_enable_(false);
	 				}
	 				//DEHUMIDIFY
	 				else if(self_.actual_rh>(self_.params.target_rh+self_.params.upper_gap_rh)){
	 					ESP_LOGI(TAG,"State: dehumidify");
	 					self_.status=APP_CONTROL_CHAMBER_DRYINGUP;
	 					humidifier_enable_(false);
	 					dryer_enable_(true);

	 				}
	 				//HUMIFY
	 				else if(self_.actual_rh<(self_.params.target_rh-self_.params.upper_gap_rh)){
	 					ESP_LOGI(TAG,"State: humidify\n");
	 					self_.status=APP_CONTROL_CHAMBER_HUMIDIFYING;
	 					humidifier_enable_(true);
	 					dryer_enable_(false);
	 				}
#endif

#if true == APP_CONTROL_EXPERIMENT_1_ENABLE
				if(self_testing_.aux_test_counter > 10){
					//EXPERIMENT
					self_testing_.id++;
					 //chamber_exp1_labels = 'id,systime,humidity_1,humidity_2,temperature_1,temperature_2,humidity_target,humidifier_on_counter,humidifier_off_counter,dryer_on_counter,dryer_off_counter'
					printf("chamber_exp_1 %d,%lld,%f,%f,%f,%f,%f,%d,%d,%d\n", self_testing_.id, esp_timer_get_time()/1000,
							api_bosch_bme280_get_humidity_last_measurement(API_BOSCH_BME280_TAG_1),api_bosch_bme280_get_humidity_last_measurement(API_BOSCH_BME280_TAG_2),
							api_bosch_bme280_get_temperature_last_measurement(API_BOSCH_BME280_TAG_1),api_bosch_bme280_get_temperature_last_measurement(API_BOSCH_BME280_TAG_1),
							self_.params.target_rh,(int)self_.status,self_.dry_ena,self_.hum_ena);

					self_testing_.aux_test_counter=0;
				}
				self_testing_.aux_test_counter++;
#endif
	 	       }
	    	 }
	       self_.status=APP_CONTROL_CHAMBER_OFF;
#if true == APP_CONTROL_EXPERIMENT_1_ENABLE
	       printf("chamber_status STOP \n");
#else
	       ESP_LOGI(TAG,"app control process OFF\n");
#endif
	       }
	      vTaskDelay(OS_APP_CONTROL_TASK_PERIOD/ portTICK_PERIOD_MS);
	    }

}

/********************** end of file ******************************************/

/** @}Final Doxygen */
