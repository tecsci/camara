/* 
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* Copyright (C) <2021>  <Martin Abel Gambarotta   magambarotta@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		app_control.h
* @date		7 Ap 2022
* @author	
*		-Lucas Mancini
* @version	v1.0.0
* @brief
*
* @{ Init Doxygen
*/

#ifndef APP_CONTROL_H_
#define APP_CONTROL_H_
/********************** inclusions *******************************************/

#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include <stdbool.h>

/********************** macros ***********************************************/

#define APP_CONTROL_EXPERIMENT_1_ENABLE (true) //for testing

#define APP_CONTROL_HUMIFIER_PIN BOARD_GPIO_PIN_6
#define APP_CONTROL_DRYER_PIN BOARD_GPIO_PIN_7

#define APP_CONTROL_USE_DEFAULT_RH_GAPS 1
//UPPER LIMIT --> 		ACTUAL > (TARGET + (NORM_UPPER_GAP - TARGET * FACTOR))
#define APP_CONTROL_UPPER_NORM_GAP_RH_DEFAULT 1
#define APP_CONTROL_UPPER_FACTOR_RH_DEFAULT 0
//LOWER LIMIT --> 		ACTUAL < (TARGET - (NORM_LOWER_GAP - TARGET * FACTOR))
#define APP_CONTROL_LOWER_NORM_GAP_RH_DEFAULT 1
#define APP_CONTROL_LOWER_FACTOR_RH_DEFAULT 0

#define APP_CONTROL_HUMIFIER_ENABLE_RANGE 2 //if((self_.params.target_rh >= self_.ambient_rh) || (self_.params.target_rh- self_.actual_rh > APP_CONTROL_HUMIFIER_ENABLE_RANGE))
#define APP_CONTROL_DRYER_ENABLE_RANGE 2 //if((self_.params.target_rh < self_.ambient_rh) || ( self_.actual_rh - self_.params.target_rh > APP_CONTROL_DRYER_ENABLE_RANGE))

/********************** typedef **********************************************/

typedef struct{

	float target_rh;

	float upper_norm_gap_rh;
	float lower_norm_gap_rh;

	float upper_factor_rh;
	float lower_factor_rh;

}app_control_params_t;

typedef enum{

	APP_CONTROL_CHAMBER_OFF = 0,
	APP_CONTROL_CHAMBER_HUMIDIFYING,
	APP_CONTROL_CHAMBER_DRYINGUP,
	APP_CONTROL_CHAMBER_BALANCED,

}app_control_status_t;
/********************** external data declaration ****************************/

extern QueueHandle_t app_control_params_queue;

extern QueueHandle_t app_control_stop_queue;

/********************** external functions declaration ***********************/
void app_control_init(void);
void app_control_deinit(void);
void app_control_loop(void *pvParameters);

#endif /* APP_CONTROL_H_ */
/** @}Final Doxygen */

