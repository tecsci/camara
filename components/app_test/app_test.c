/*
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		app_test.c
* @date		14 jun. 2021
* @author	
*		-Martin Abel Gambarotta   magambarotta@gmail.com
* @version	v1.0.0
* @brief
*
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "app_test.h"
#include "api.h"
#include "os.h"
#include <stdbool.h>

#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include <stdbool.h>

/********************** macros and definitions *******************************/

#define TEST_BLINK 0
#define SERIAL_0_TEST 0
#define TEST_APP_CONTROL 1
#define TEST_APP_CONTROL_2 0

#define TEST_APP_CONTROL_INIT_RH 10
#define TEST_APP_CONTROL_MAX_RH 90
#define TEST_APP_CONTROL_INCREMENT_RH 10
#define TEST_APP_CONTROL_TARGET_TIME_SEGS 300 //segs = 5min



/********************** internal data declaration ****************************/
static const char *TAG = "app_test";
/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/
void app_test_init(void)
{
	xTaskCreate(app_test_loop, "test task" 	// A name just for humans
			, OS_APP_TEST_TASK_SIZE 			// This stack size can be checked & adjusted by reading the Stack Highwater
			, NULL, OS_APP_TEST_TASK_PRIORITY 		// Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
			, NULL);


}
void app_test_deinit(void){
}
void app_test_loop(void *pvParameters)
{
	ESP_LOGI(TAG,"%s",__func__);
#if 1== TEST_APP_CONTROL
  static app_control_params_t 		aux_params;
  static uint32_t aux_counter= 		0 ;
  static bool dummy_send = 			true;
  static bool first_send_done = 	false;
  aux_params.target_rh = 			TEST_APP_CONTROL_INIT_RH;
  static bool humidity_up = 		true;

#endif

#if 1 == TEST_APP_CONTROL_2
  static app_control_params_t aux_params;
  aux_params.target_rh = 60;
  vTaskDelay(2000/ portTICK_PERIOD_MS);
  xQueueGenericSend(app_control_params_queue, & aux_params.target_rh,0U, 0U);
#endif

#if 1 == TEST_BLINK
  static bool state=true;
#endif

  #if SERIAL_1_TEST
  uint8_t tx,rx;
  tx= 0x69;
  rx=0x03;
#endif



for(;;) {

      //TEST BLINK
#if 1 == TEST_BLINK
      state=!state;
      api_gpio_write(API_GPIO_PIN_12, state);

#endif

      //TEST UART_1
#if 1 == SERIAL_1_TEST
      board_serial_write(API_UART_1,"HOLA HECTOR DESDE UART1\r\n");
      //api_serial_is_available(API_UART_1);
#endif

      //TEST UART_0
#if 1== SERIAL_0_TEST
      ESP_LOGI(TAG,"blink");
#endif

      //APP CONTROL TEST
#if 1== TEST_APP_CONTROL

      if(false == first_send_done){
    	  aux_params.target_rh = TEST_APP_CONTROL_INIT_RH;
        	if(NULL != app_control_params_queue ){
        		ESP_LOGI(TAG,"send  start queue");
        		xQueueGenericSend(app_control_params_queue, & aux_params.target_rh,0U, 0U);
          	  first_send_done = true;
          	  aux_counter = 0;
        	}
      }

    if(aux_counter >= TEST_APP_CONTROL_TARGET_TIME_SEGS ){ //T_TOTAL = TEST_APP_CONTROL_TARGET_TIME_SEGS*OS_APP_TEST_TASK_PERIOD

      	if(NULL != app_control_stop_queue ){
      		ESP_LOGI(TAG,"send  stop queue");
      		xQueueGenericSend(app_control_stop_queue, &dummy_send,0U, 0U);
      	}

      	vTaskDelay(OS_APP_TEST_TASK_PERIOD/ portTICK_PERIOD_MS);


      	if (true == humidity_up){
      		aux_params.target_rh += TEST_APP_CONTROL_INCREMENT_RH;
    		if (TEST_APP_CONTROL_MAX_RH == aux_params.target_rh ){
    			humidity_up= false;
    		}
      	}
      	else if(false == humidity_up){
      		aux_params.target_rh -= TEST_APP_CONTROL_INCREMENT_RH;
        	if (TEST_APP_CONTROL_INIT_RH == aux_params.target_rh ){
        		humidity_up= true;
        	}
      	}


      	if(NULL != app_control_params_queue ){
      		ESP_LOGI(TAG,"send queue: target rh %f", aux_params.target_rh);
      		xQueueGenericSend(app_control_params_queue, &aux_params,0U, 0U);
      	}
    	//DICREASE TARGET RH


#if 0
      	if(aux_params.target_rh > TEST_APP_CONTROL_MAX_RH){
    		aux_params.target_rh = TEST_APP_CONTROL_INIT_RH;
    	}
#endif

      	//RESTART COUNTER
    	aux_counter = 0;
    }
    //INCREASE COUNTER
	aux_counter++;
#endif


      vTaskDelay(OS_APP_TEST_TASK_PERIOD/ portTICK_PERIOD_MS);
 }

}

/********************** end of file ******************************************/

/** @}Final Doxygen */
