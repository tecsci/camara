/* 
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* Copyright (C) <2021>  <Martin Abel Gambarotta   magambarotta@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		app_test.h
* @date		14 jun. 2021
* @author	
*		-martin
* @version	v1.0.0
* @brief
*
* @{ Init Doxygen
*/

#ifndef COMPONENTS_APP_INCLUDE_APP_TEST_H_
#define COMPONENTS_APP_INCLUDE_APP_TEST_H_
/********************** inclusions *******************************************/

#include "app_control.h"

/********************** macros ***********************************************/


/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/
void app_test_init(void);
void app_test_deinit(void);
void app_test_loop(void *pvParameters);

#endif /* COMPONENTS_APP_INCLUDE_APP_TEST_H_ */
/** @}Final Doxygen */

