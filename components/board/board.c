/*
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		board.c
* @date		15 jun. 2021
* @author	
*		-Martin Abel Gambarotta   (magambarotta@gmail.com)
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "board.h"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/
void board_init(void)
{
  board_gpio_init();
  board_serial_init();
  board_delay_init();
  board_timer_init();
  board_mac_init();
  board_i2c_init();
  board_pid_init();
#if 0
  //board_spi_init();
  board_wifi_init();
#endif

}
void board_deinit(void)
{
  board_serial_deinit();
  board_gpio_deinit();
  board_delay_deinit();
  board_timer_deinit();
  board_mac_deinit();
  board_i2c_deinit();
  board_pid_deinit();
#if 0
  // board_spi_deinit();
  board_wifi_deinit();
#endif

}
void board_loop(void)
{
  board_gpio_loop();
  board_serial_loop();
  board_delay_loop();
  board_timer_loop();
  board_mac_loop();
  board_i2c_loop();
  board_pid_loop();
#if 0
  //board_spi_loop();
  board_wifi_loop();
#endif
}


/********************** end of file ******************************************/

/** @}Final Doxygen */
