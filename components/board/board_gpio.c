/*
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* Copyright (C) <2021>  <Martin Abel Gambarotta   magambarotta@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		board_gpio.c
* @date		14 jun. 2021
* @author	
*		-Martin Abel Gambarotta   magambarotta@gmail.com
* @version	v1.0.0
* @brief
*
* @{ Init Doxygen
*/


/********************** inclusions *******************************************/
#include "board_gpio.h"

#include "esp_err.h"
#include "driver/gpio.h"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/
void board_gpio_config( board_gpio_config_t mode, board_gpio_pin_t pin)
{
  /* Configure the IOMUX register for pad BLINK_GPIO (some pads are
     muxed to GPIO on reset already, but some default to other
     functions and need to be switched to GPIO. Consult the
     Technical Reference for a list of pads and their default
     functions.)
  */

  gpio_pad_select_gpio(pin);

  switch (mode) {
    case BOARD_GPIO_INPUT:
      gpio_set_direction(pin, GPIO_MODE_INPUT);
      break;
    case BOARD_GPIO_OUTPUT:
      gpio_set_direction(pin, GPIO_MODE_OUTPUT);
      break;
    default:
      //gpio_set_direction(pin, GPIO_MODE_DISABLE);;
      break;
  }



}
bool board_gpio_read(board_gpio_pin_t pin)
{
  return gpio_get_level (pin);
}
bool board_gpio_write (board_gpio_pin_t pin, bool state)
{
  if (ESP_OK == gpio_set_level (pin, state))
    {
      return true;
    }
  else
    {
      return false;
    }
}


void board_gpio_init (void)
{

  board_gpio_config(BOARD_GPIO_OUTPUT, BOARD_GPIO_PIN_6);
  board_gpio_config(BOARD_GPIO_OUTPUT, BOARD_GPIO_PIN_7);

  board_gpio_write(BOARD_GPIO_PIN_6,false);
  board_gpio_write(BOARD_GPIO_PIN_7,false);

}
void board_gpio_deinit (void)
{
}
void board_gpio_loop (void)
{
}


/********************** end of file ******************************************/

/** @}Final Doxygen */
