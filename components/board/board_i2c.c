/*
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		board_i2c.c
* @date		22 sep. 2021
* @author	
*		-Martin Abel Gambarotta   <magambarotta@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "board_i2c.h"
#include "hardware.h"
/********************** macros and definitions *******************************/

#define BOARD_I2C_0_SDA HARDWARE_ESP32_I2C_0_SDA
#define BOARD_I2C_0_SCL HARDWARE_ESP32_I2C_0_SCL


#define BOARD_I2C_0_MASTER_FREQ_HZ 	(100000)
#define BOARD_I2C_MASTER_TX_BUF_DISABLE 0        		/*!< I2C master doesn't need buffer */
#define BOARD_I2C_MASTER_RX_BUF_DISABLE 0         		/*!< I2C master doesn't need buffer */


#define BOARD_I2C_ESP_SLAVE_ADDR		(0x28)				/*!< ESP32 slave address, you can set any 7bit value */


#define ACK_CHECK_EN 0

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/
static const char *TAG = "board_i2c";

static struct {

	i2c_config_t config_master,config_slave;
	i2c_cmd_handle_t cmd;

}self;
/********************** external data definition *****************************/

/********************** internal functions definition ************************/
static bool board_i2c_master_config_() {

	int i2c_master_port = 0;

	self.config_master.mode = I2C_MODE_MASTER;
	self.config_master.sda_io_num = BOARD_I2C_0_SDA;
	self.config_master.sda_pullup_en = GPIO_PULLUP_ENABLE;
	self.config_master.scl_io_num = BOARD_I2C_0_SCL;
	self.config_master.scl_pullup_en = GPIO_PULLUP_ENABLE;
	self.config_master.master.clk_speed = BOARD_I2C_0_MASTER_FREQ_HZ;
	//self.config_master.master .clk_flags = 0,

	if (ESP_OK != i2c_param_config(i2c_master_port, &self.config_master)) {
		ESP_LOGE(TAG, "ERROR CONFIG I2C");
		return false;
	}

	else if ( ESP_OK != i2c_driver_install(i2c_master_port, self.config_master.mode,BOARD_I2C_MASTER_RX_BUF_DISABLE,BOARD_I2C_MASTER_TX_BUF_DISABLE, 0)) {
		ESP_LOGE(TAG, "ERROR CONFIG I2C");
		return false;
	} else
		return true;

}


static bool board_i2c_slave_config_() {
#if 0
	int i2c_slave_port = 0;

	self.config_slave.mode = I2C_MODE_SLAVE;
	self.config_slave.sda_io_num = BOARD_ESP32_I2C_0_SDA; // select GPIO specific to your project
	self.config_slave.sda_pullup_en = GPIO_PULLUP_ENABLE;
	self.config_slave.scl_io_num = BOARD_ESP32_I2C_0_SCL; // select GPIO specific to your project
	self.config_slave.scl_pullup_en = GPIO_PULLUP_ENABLE;
	//self.config_slave.clk_flags = 0,          /*!< Optional, you can use I2C_SCLK_SRC_FLAG_* flags to choose i2c source clock here. */
	if (ESP_OK != i2c_param_config(i2c_slave_port, &self.config_master)) {
		ESP_LOGE(TAG, "ERROR CONFIG I2C");
		return false;
	}
	else if ( ESP_OK != i2c_driver_install(i2c_slave_port, self.config_master.mode,BOARD_I2C_MASTER_RX_BUF_DISABLE,BOARD_I2C_MASTER_TX_BUF_DISABLE, 0)) {
		ESP_LOGE(TAG, "ERROR CONFIG I2C");
		return false;
	} else
		return true;
#endif

	return false;
}

/********************** external functions definition ************************/

uint8_t board_i2c_master_write(board_i2c_id_t i2c_n,uint8_t device_address,uint8_t reg_addr, uint8_t *data, uint32_t data_len){

	int8_t rslt = 0; // Return 0 for Success, non-zero for failure
	esp_err_t err;

#if 0
	uint8_t dev_addr = *((uint8_t *) intf_ptr);
#endif

	i2c_cmd_handle_t cmd = i2c_cmd_link_create();

	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (device_address << 1) | I2C_MASTER_WRITE, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, reg_addr, true);
	i2c_master_write(cmd, data, data_len, ACK_CHECK_EN);
	i2c_master_stop(cmd);

	err = i2c_master_cmd_begin(0, cmd, 10/portTICK_PERIOD_MS);
	if (err != ESP_OK) {
		rslt = -1;
		ESP_LOGE(TAG,"ERROR I2C BEGIN");
	}
	i2c_cmd_link_delete(cmd);

	return rslt;


}

uint8_t board_i2c_master_read(board_i2c_id_t i2c_n,uint8_t device_address,uint8_t reg_addr,uint8_t *data, uint32_t data_len) {


	int8_t rslt = 0; /* Return 0 for Success, non-zero for failure */
	esp_err_t espRc;


	i2c_cmd_handle_t cmd = i2c_cmd_link_create();

	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (device_address << 1) | I2C_MASTER_WRITE, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, reg_addr, ACK_CHECK_EN);


	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (device_address << 1) | I2C_MASTER_READ, ACK_CHECK_EN);

	if (data_len > 1) {
		i2c_master_read(cmd, data, data_len-1, I2C_MASTER_ACK);
	}
	i2c_master_read_byte(cmd, data+data_len-1, I2C_MASTER_NACK);
	i2c_master_stop(cmd);

	espRc = i2c_master_cmd_begin(i2c_n, cmd, 10/portTICK_PERIOD_MS);
	if (espRc != ESP_OK) {
		rslt = -1; //ERROR
	}

	i2c_cmd_link_delete(cmd);

	return rslt;

}




void board_i2c_init(void){
	board_i2c_master_config_();
}
void board_i2c_deinit(){

}
void board_i2c_loop(){

}

/********************** end of file ******************************************/

/** @}Final Doxygen */
