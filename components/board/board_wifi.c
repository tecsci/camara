/*
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		board_wifi.c
* @date		8 nov. 2021
* @author	
*		-Lucas Mancini <mancinilucas95@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

/******** inclusions ***************/

#include "include/board_wifi.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

#include "esp_log.h"
#include "esp_event.h"
#include "nvs_flash.h"
#include "driver/gpio.h"
#include "esp_netif.h"
#include "esp_wifi.h"

/******** macros and definitions ***********/

#define BOARD_WIFI_AUTORECONNECT_FUNC 1
#define BOARD_WIFI_AUTORECONNECT_MAX 10

/******** internal data declaration **********/
static struct {

	bool is_connected;
#if 1 == BOARD_WIFI_AUTORECONNECT_FUNC
	bool autoreconnect;
	uint16_t n_autoreconnect;
#endif

}self_;

/******** internal data definition ***********/

static const char *TAG = "board_wifi";

/******** external data definition ***********/

/******** internal functions definition ********/
static void event_handler_(void* arg, esp_event_base_t event_base,
                                int32_t event_id, void* event_data)
{
	if(event_base == WIFI_EVENT){
		ESP_LOGI(TAG, "wifi event");

		switch (event_id) {
			case WIFI_EVENT_STA_START:
				ESP_LOGI(TAG, "--> start");
				break;
			case WIFI_EVENT_STA_CONNECTED:
				ESP_LOGI(TAG, "--> connected");
				self_.is_connected=true;
#if 1 == BOARD_WIFI_AUTORECONNECT_FUNC
				self_.autoreconnect=true;
				self_.n_autoreconnect = 0;
#endif
				//wifi_event_sta_connected_t c_data = * ((wifi_event_sta_connected_t*) event_data);
				break;
			case WIFI_EVENT_STA_DISCONNECTED:
				ESP_LOGI(TAG, "--> disconnected");
				self_.is_connected=false;
#if 1 == BOARD_WIFI_AUTORECONNECT_FUNC
				if(true == self_.autoreconnect){

					if(self_.n_autoreconnect < BOARD_WIFI_AUTORECONNECT_MAX ){
						board_wifi_sta_reconnect();
						self_.n_autoreconnect++;
					}
					else{
						self_.autoreconnect=false;
					}
				}
#endif
				//wifi_event_sta_disconnected_t c_data = * ((wifi_event_sta_connected_t*) event_data);
				break;
			case WIFI_EVENT_SCAN_DONE:
				ESP_LOGI(TAG, "--> scan done");
				break;
			default:
				ESP_LOGI(TAG, "--> otro");
				break;
		}
	}
}

/******** external functions definition ********/


void board_wifi_scan_print(board_wifi_record_t* p_ap_records, uint16_t ap_num){

	   // print the list
	   printf("Found %d access points:\n", ap_num);

	   for(int i = 0; i < ap_num; i++){
		   ESP_LOGI(TAG, "Name %d: %32s", i , p_ap_records[i].ssid);
	   }

#if 0
	   printf("               SSID              | Channel | RSSI |   MAC \n\n");
	   //printf("----------------------------------------------------------------\n");
	   for(int i = 0; i < ap_num; i++)
	     printf("%32s | %7d | %4d   %2x:%2x:%2x:%2x:%2x:%2x   \n",
	    		 ap_records[i].ssid, ap_records[i].primary, ap_records[i].rssi
				 , *ap_records[i].bssid, *(ap_records[i].bssid+1), *(ap_records[i].bssid+2),
				 *(ap_records[i].bssid+3), *(ap_records[i].bssid+4), *(ap_records[i].bssid+5));
#endif
}

uint16_t board_wifi_scan(board_wifi_record_t* p_ap_records, uint16_t size){

#if 0
		wifi_scan_config_t scan_config = {
			.ssid = 0,  /**< SSID of AP */
			.bssid = 0,  /**< MAC address of AP */
			.channel = 0,   /**< channel, scan the specific channel */
			.show_hidden = false   /**< enable to scan AP whose SSID is hidden */
		    wifi_scan_type_t scan_type;  /**< scan type, active or passive */
		    wifi_scan_time_t scan_time;  /**< scan time per channel */
		};
#endif

	   ESP_ERROR_CHECK(
			   esp_wifi_scan_start(NULL, true));


	   uint16_t ap_num;
	   ESP_ERROR_CHECK(
			   esp_wifi_scan_get_ap_num(&ap_num));

	   if(ap_num > size){
		   return 0;
	   }

	   ESP_ERROR_CHECK(
			   esp_wifi_scan_get_ap_records(&ap_num, p_ap_records));

	   return ap_num;
 }

bool  board_wifi_sta_connect(char* ssid, char* pass){
	  wifi_config_t sta_config;
	  strcpy((char*) sta_config.sta.ssid,ssid);
	  strcpy((char*) sta_config.sta.password,pass);
	  sta_config.sta.bssid_set=false;
	  sta_config.sta.threshold.authmode = WIFI_AUTH_OPEN;//WIFI_AUTH_WPA2_PSK;
	  sta_config.sta.pmf_cfg.capable = true;
	  sta_config.sta.pmf_cfg.required = false;

	  if(ESP_OK == esp_wifi_set_mode(WIFI_MODE_STA)){
		  if(ESP_OK == esp_wifi_set_config(WIFI_IF_STA, &sta_config)){
			  if(ESP_OK == esp_wifi_start()){
				  if(ESP_OK == esp_wifi_connect()){
					  self_.is_connected= true;
					  return true;
				  }
			  }
		  }
	  }

	    return false;
}

bool  board_wifi_sta_reconnect(void){
	  if(ESP_OK != esp_wifi_connect()){
		  self_.is_connected = true;
		  return true;
	  }
	  return false;
}

void  board_wifi_sta_enable_autoreconnect(bool enable){
	self_.autoreconnect=enable;
}

bool  board_wifi_sta_disconnect(void){
	  if(ESP_OK != esp_wifi_disconnect()){
		  self_.is_connected=false;
		  return true;
	  }
	  return false;
}

bool  board_wifi_sta_is_connected(void){

	return self_.is_connected;
}

void board_wifi_init(){

	ESP_LOGI(TAG,"%s",__func__);
	esp_event_handler_instance_t instance_any_id;
	self_.is_connected=false;

    //Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    //NETIF INIT
#if 0
	esp_netif_t *sta_netif = esp_netif_create_default_wifi_sta();
    assert(sta_netif);
#endif

    esp_event_loop_create_default();

    //WIFI CONFIG INIT
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();

    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    //REgister Event
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                            ESP_EVENT_ANY_ID,
                                                            &event_handler_,
                                                            NULL,
                                                            &instance_any_id));

    esp_wifi_set_mode(WIFI_MODE_STA);


    ESP_ERROR_CHECK(
    		esp_wifi_start());
#if 0
    ESP_ERROR_CHECK( esp_wifi_connect() );
#endif

}

void board_wifi_deinit(){

}
void board_wifi_loop(){

}

/******** end of file **************/

/** @}Final Doxygen */
