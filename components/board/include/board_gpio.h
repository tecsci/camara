/*
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* Copyright (C) <2021>  <Martin Abel Gambarotta   magambarotta@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		board_gpio.h
* @date		14 jun. 2021
* @author	
*		-martin
* @version	v1.0.0
* @brief
*
* @{ Init Doxygen
*/
#ifndef COMPONENTS_BOARD_BOARD_GPIO_H_
#define COMPONENTS_BOARD_BOARD_GPIO_H_

/********************** inclusions *******************************************/
#include "stdbool.h"
#include "hardware.h"

/********************** macros ***********************************************/

#define GPIO_OUTPUT_PIN_1 HARDWARE_ESP32_GPIO_1
#define GPIO_OUTPUT_PIN_2 HARDWARE_ESP32_GPIO_2
#define GPIO_OUTPUT_PIN_3 HARDWARE_ESP32_GPIO_3
#define GPIO_OUTPUT_PIN_4 HARDWARE_ESP32_GPIO_4
#define GPIO_OUTPUT_PIN_5 HARDWARE_ESP32_GPIO_5
#define GPIO_OUTPUT_PIN_6 HARDWARE_ESP32_GPIO_6
#define GPIO_OUTPUT_PIN_12 HARDWARE_ESP32_GPIO_12

#define GPIO_INPUT_PIN_1 HARDWARE_ESP32_GPIO_7
#define GPIO_INPUT_PIN_2 HARDWARE_ESP32_GPIO_8
#define GPIO_INPUT_PIN_3 HARDWARE_ESP32_GPIO_9
#define GPIO_INPUT_PIN_4 HARDWARE_ESP32_GPIO_10
#define GPIO_INPUT_PIN_5 HARDWARE_ESP32_GPIO_11

/********************** typedef **********************************************/
typedef enum
{
  BOARD_GPIO_INPUT,
  BOARD_GPIO_INPUT_PULLDOWN,
  BOARD_GPIO_INPUT_PULLUP,
  BOARD_GPIO_OUTPUT,
  BOARD_GPIO_OUTPUT_PULLDOWN,
  BOARD_GPIO_OUTPUT_PULLUP,
  BOARD_GPIO_INPUT_OUTPUT

}board_gpio_config_t;

typedef enum
{
  BOARD_GPIO_PIN_1 = GPIO_INPUT_PIN_1,
  BOARD_GPIO_PIN_2 = GPIO_INPUT_PIN_2,
  BOARD_GPIO_PIN_3 = GPIO_INPUT_PIN_3,
  BOARD_GPIO_PIN_4 = GPIO_INPUT_PIN_4,
  BOARD_GPIO_PIN_5 = GPIO_INPUT_PIN_5,

  BOARD_GPIO_PIN_6 = GPIO_OUTPUT_PIN_1,
  BOARD_GPIO_PIN_7 = GPIO_OUTPUT_PIN_2,
  BOARD_GPIO_PIN_8 = GPIO_OUTPUT_PIN_3,
  BOARD_GPIO_PIN_9 = GPIO_OUTPUT_PIN_4,
  BOARD_GPIO_PIN_10= GPIO_OUTPUT_PIN_5,
  BOARD_GPIO_PIN_11 = GPIO_OUTPUT_PIN_6,
  BOARD_GPIO_PIN_12 = GPIO_OUTPUT_PIN_12,

}board_gpio_pin_t;


/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/
//TODO hacer estatica eh interna esta funcion
void board_gpio_config(board_gpio_config_t config,board_gpio_pin_t pin);
bool board_gpio_read(board_gpio_pin_t pin);
bool board_gpio_write(board_gpio_pin_t pin, bool enable);

void board_gpio_init(void);
void board_gpio_deinit(void);
void board_gpio_loop(void);

#endif /* COMPONENTS_BOARD_BOARD_GPIO_H_ */
/** @}Final Doxygen */
