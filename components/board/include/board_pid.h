/*
 * Copyright (c) 2021 Lucas Mancini <mancinilucas95@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @file	board_pid.cpp
 * @date	2021-19-MAY
 * @author:
 *  - Lucas Mancini <mancinilucas95@gmail.com>
 * @version	v1.0.0
 *
 *
 * Library adapted to ESP32 (in C)
 * by Lucas Mancini
 * lucasmancini95@gmail.com
 * Original Library:
 * Arduino PID Library - Version 1.2.1
 * by Brett Beauregard <br3ttb@gmail.com> brettbeauregard.com
 * This Library is licensed under the MIT License
*/

#ifndef _BOARD_PID_HPP_
#define _BOARD_PID_HPP_

/********************** inclusions *******************************************/
#include <stdio.h>
#include <stdbool.h>

/********************** macros ***********************************************/

#define BOARD_PID_AUTOMATIC_FLAG	1
#define BOARD_PID_MANUAL_FLAG	0
#define BOARD_PID_DIRECT_FLAG  0
#define BOARD_PID_REVERSE_FLAG   1
#define BOARD_PID_PROPORTIONAL_ON_MEASUREMENT 0 //P_ON_M
#define BOARD_PID_PROPORTIONAL_ON_ERROR 1 //P_ON_E

/********************** typedef **********************************************/

typedef struct{

	double kp;                  // * (P)roportional Tuning Parameter
	double ki;                  // * (I)ntegral Tuning Parameter
	double kd;                  // * (D)erivative Tuning Parameter

	double dispKp;				//kp for display
	double dispKi;				//ki for display
	double dispKd;				//kd for display

	int controllerDirection;
	int pOn;

	double *myInput;              // * Pointers to the Input, Output, and Setpoint variables
	double *myOutput;             //   This creates a hard link between the variables and the
	double *mySetpoint;           //   PID, freeing the user from having to constantly tell us
								  //   what these values are.  with pointers we'll just know.

	unsigned long lastTime;
	double outputSum, lastInput;

	unsigned long SampleTime;
	double outMin, outMax;
	bool inAuto, pOnE;

}board_pid_t;

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void board_pid_init(void);
void board_pid_deinit(void);
void board_pid_loop(void);

//PID init function
void board_pid_Initialize(board_pid_t* p_PID);

void board_pid_Constructor(board_pid_t* p_PID, double* Input, double* Output, double* Setpoint,
        double Kp, double Ki, double Kd, int POn, int ControllerDirection);

#if 0 //not in use
 void board_pid_constructor_simple(board_pid_t* p_PID, double* Input, double* Output, double* Setpoint,
                 double Kp, double Ki, double Kd, int ControllerDirection);
#endif

void board_pid_SetMode(board_pid_t* p_PID, int Mode); //sets PID to either Manual (0) or Auto (non-0)

bool board_pid_Compute(board_pid_t* p_PID); //performs the PID calculation.  it should be called every time loop() cycles.
void board_pid_SetOutputLimits(board_pid_t* p_PID, double, double); //clamps the output to a specific range. 0-255

//available but not commonly used functions
void board_pid_SetTunings(board_pid_t* p_PID, double Kp, double Ki, double Kd, int POn);  // gives the option of changing tunings during runtime
void board_pid_SetTunings_simple(board_pid_t* p_PID,double Kp, double Ki, double Kd);  // overload for specifying proportional mode
void board_pid_SetControllerDirection(board_pid_t* p_PID,int Direction); //Sets the Direction, or "Action" of the controller.
																		//DIRECT means the output will increase when error is positive.
																		//REVERSE means the opposite.
void board_pid_SetSampleTime(board_pid_t* p_PID,int NewSampleTime);  // sets the frequency (in ms) with which the PID calculation is performed.(default 100)


#if 0 //NOT IN USE
//Display functions ****************************************************************
 double PID_GetKp(PID_t* p_PID);
 double PID_GetKi(PID_t* p_PID);
 double PID_GetKd(PID_t* p_PID);
 int PID_GetMode(PID_t* p_PID);
 int PID_GetDirection(PID_t* p_PID);

//PID original class - Cpp
class PID
{
  public:
    //Constants used in some of the functions below
    #define AUTOMATIC	1
    #define MANUAL	0
    #define DIRECT  0
    #define REVERSE  1
    #define P_ON_M 0
    #define P_ON_E 1
  //commonly used functions **************************************************************************
    PID(double*, double*, double*,        // * constructor.  links the PID to the Input, Output, and
        double, double, double, int, int);//   Setpoint.  Initial tuning parameters are also set here.
                                          //   (overload for specifying proportional mode)
    PID(double*, double*, double*,        // * constructor.  links the PID to the Input, Output, and
        double, double, double, int);     //   Setpoint.  Initial tuning parameters are also set here
    void SetMode(int Mode);               // * sets PID to either Manual (0) or Auto (non-0)
    bool Compute();                       // * performs the PID calculation.  it should be
                                          //   called every time loop() cycles. ON/OFF and
                                          //   calculation frequency can be set using SetMode
                                          //   SetSampleTime respectively
    void SetOutputLimits(double, double); // * clamps the output to a specific range. 0-255 by default, but
										                      //   it's likely the user will want to change this depending on
										                      //   the application
  //available but not commonly used functions ********************************************************
    void SetTunings(double, double,       // * While most users will set the tunings once in the
                    double);         	    //   constructor, this function gives the user the option
                                          //   of changing tunings during runtime for Adaptive control
    void SetTunings(double, double,       // * overload for specifying proportional mode
                    double, int);
	void SetControllerDirection(int);	  // * Sets the Direction, or "Action" of the controller. DIRECT
										  //   means the output will increase when error is positive. REVERSE
										  //   means the opposite.  it's very unlikely that this will be needed
										  //   once it is set in the constructor.
    void SetSampleTime(int);              // * sets the frequency, in Milliseconds, with which
                                          //   the PID calculation is performed.  default is 100
  //Display functions ****************************************************************
	double GetKp();						  // These functions query the pid for interal values.
	double GetKi();						  //  they were created mainly for the pid front-end,
	double GetKd();						  // where it's important to know what is actually
	int GetMode();						  //  inside the PID.
	int GetDirection();					  //
  private:
	void Initialize();
	double dispKp;				// * we'll hold on to the tuning parameters in user-entered
	double dispKi;				//   format for display purposes
	double dispKd;				//
	double kp;                  // * (P)roportional Tuning Parameter
    double ki;                  // * (I)ntegral Tuning Parameter
    double kd;                  // * (D)erivative Tuning Parameter
	int controllerDirection;
	int pOn;
    double *myInput;              // * Pointers to the Input, Output, and Setpoint variables
    double *myOutput;             //   This creates a hard link between the variables and the
    double *mySetpoint;           //   PID, freeing the user from having to constantly tell us
                                  //   what these values are.  with pointers we'll just know.
	unsigned long lastTime;
	double outputSum, lastInput;
	unsigned long SampleTime;
	double outMin, outMax;
	bool inAuto, pOnE;
};
#endif

#endif /*_BOARD_PID_HPP_*/
/********************** end of file ******************************************/
