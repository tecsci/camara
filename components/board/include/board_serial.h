/* 
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		board_serial.h
* @date		14 jun. 2021
* @author	
*		-martin
* @version	v1.0.0
* @brief
*
* @{ Init Doxygen
*/

#ifndef COMPONENTS_BOARD_INCLUDE_BOARD_SERIAL_H_
#define COMPONENTS_BOARD_INCLUDE_BOARD_SERIAL_H_
/********************** inclusions *******************************************/
#include "stdint.h"
/********************** macros ***********************************************/

/********************** typedef **********************************************/

typedef enum{

	BOARD_UART_0=0, //console
	BOARD_UART_1=1,
	BOARD_UART_2=2

}board_uart_id_t;

/********************** external data declaration ****************************/
/********************** external functions declaration ***********************/

void board_serial_setup(board_uart_id_t uart_id);
void board_serial_write(board_uart_id_t uart_id,void* buffer,uint32_t len );
uint32_t board_serial_read(board_uart_id_t uart_id ,void* buffer);
uint32_t board_serial_is_available(board_uart_id_t uart_id);


void board_serial_init(void);
void board_serial_deinit(void);
void board_serial_loop(void);

#endif /* COMPONENTS_BOARD_INCLUDE_BOARD_SERIAL_H_ */
/** @}Final Doxygen */

