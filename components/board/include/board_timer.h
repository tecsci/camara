/* 
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		board_timer.h
* @date		19 oct. 2021
* @author	
*		-Lucas Mancini <mancinilucas95@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef _BOARD_TIMER_H_
#define _BOARD_TIMER_H_
/******** inclusions ***************/

#include "stdlib.h"
#include "stdint.h"
#include "unistd.h"

/******** macros *****************/

/******** typedef ****************/

/******** external data declaration **********/

/******** external functions declaration *********/

uint32_t board_timer_get_time_ms(void);

void board_timer_init();
void board_timer_deinit();
void board_timer_loop();

#endif /* _BOARD_TIMER_H_ */
/** @}Final Doxygen */
