/* 
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		board_wifi.h
* @date		8 nov. 2021
* @author	
*		-Lucas Mancini <mancinilucas95@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef _BOARD_WIFI_H_
#define _BOARD_WIFI_H_
/******** inclusions ***************/

#include "esp_wifi.h"

#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "stdint.h"
#include "unistd.h"
#include "stdbool.h"

/******** macros *****************/

/******** typedef ****************/

typedef wifi_ap_record_t board_wifi_record_t;

/******** external data declaration **********/

/******** external functions declaration *********/

void board_wifi_scan_print(board_wifi_record_t* p_ap_records, uint16_t ap_num);
uint16_t board_wifi_scan(board_wifi_record_t* p_ap_records, uint16_t size);
bool  board_wifi_sta_connect(char* ssid, char* pass);
bool  board_wifi_sta_reconnect(void);
bool  board_wifi_sta_disconnect(void);
bool  board_wifi_sta_is_connected(void);

void board_wifi_init();
void board_wifi_deinit();
void board_wifi_loop();

#endif /* _BOARD_WIFI_H_ */
/** @}Final Doxygen */
