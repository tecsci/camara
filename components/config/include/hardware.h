/* 
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		hardware.h
* @date		15 jun. 2021
* @author	
*		-Martin Abel Gambarotta   (magambarotta@gmail.com)
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef COMPONENTS_CONFIG_INCLUDE_HARDWARE_H_
#define COMPONENTS_CONFIG_INCLUDE_HARDWARE_H_
/********************** inclusions *******************************************/

/********************** macros ***********************************************/

//GPIO  EXTERNALIZADOS

//J204  Tira de Pines   OUTPUT
#define HARDWARE_ESP32_GPIO_1 		(26)
#define HARDWARE_ESP32_GPIO_2 		(25)
#define HARDWARE_ESP32_GPIO_3 		(33)
#define HARDWARE_ESP32_GPIO_4 		(32)
#define HARDWARE_ESP32_GPIO_5 		(35)
#define HARDWARE_ESP32_GPIO_6 		(34)


//Cambia segun modulo de TEST
#define HARDWARE_ESP32_GPIO_12 		(2)

//J203  Tira de Pines    INPUT
#define HARDWARE_ESP32_GPIO_7 		(17)
#define HARDWARE_ESP32_GPIO_8 		(5)
#define HARDWARE_ESP32_GPIO_9 		(18)
#define HARDWARE_ESP32_GPIO_10 		(19)
#define HARDWARE_ESP32_GPIO_11 		(21)


//SPI 2      SPI0 and SPI1 --> for flash memory      SPI3 Free
#define HARDWARE_ESP32_SPI_2_SCK	(12)		//GPIO
#define HARDWARE_ESP32_SPI_2_MISO 	(27)            //GPIO
#define HARDWARE_ESP32_SPI_2_MOSI 	(14)            //GPIO
#define HARDWARE_ESP32_SPI_2_CS 	(15)   		//GPIO en manual usan I05 (no hay problema con este cambio)

#define HARDWARE_ESP32_SPI_2_MAX_TRANSFER_SIZE (40)  	//Bytes

//UART 0
#define HARDWARE_ESP32_UART_0_RX 	(3)
#define HARDWARE_ESP32_UART_0_TX 	(1)

//UART 1 Default   (  RX-9   Tx-10)
#define HARDWARE_ESP32_UART_1_RX 	(25)
#define HARDWARE_ESP32_UART_1_TX 	(26)


//UART 2  GPIO 16 usado por integrado TMC5130
//#define HARDWARE_ESP32_UART_2_RX 	(16)
//#define HARDWARE_ESP32_UART_2_TX 	(17)

//I2C
#define  HARDWARE_ESP32_I2C_0_SDA	(18)
#define  HARDWARE_ESP32_I2C_0_SCL	(19)
//#define  HARDWARE_ESP32_I2C_0_SDA	(21)
//#define  HARDWARE_ESP32_I2C_0_SCL	(22)


/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

#endif /* COMPONENTS_CONFIG_INCLUDE_HARDWARE_H_ */
/** @}Final Doxygen */









