/* 
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		os.h
* @date		27 oct. 2021
* @author	
*	-Martin Abel Gambarotta   (magambarotta@gmail.com)
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef _COMPONENTS_CONFIG_INCLUDE_OS_H_
#define _COMPONENTS_CONFIG_INCLUDE_OS_H_
/********************** TASKS CONFIG *******************************************/

//TASK PRIORITIES
#define OS_APP_TEST_TASK_PRIORITY (2)
#define OS_APP_CONTROL_TASK_PRIORITY (2)
#define OS_APP_CONSOLE_TASK_PRIORITY (2)

//TASKS LOOP PERIOD
#define OS_APP_TEST_TASK_PERIOD					(1000)
#define OS_APP_CONTROL_TASK_PERIOD				(100)

//TASKS SIZES
#define OS_APP_TEST_TASK_SIZE								(4096)
#define OS_APP_CONTROL_TASK_SIZE 								(4096)
#define OS_APP_CONSOLE_TASK_SIZE  					  (4096*3)

//QUEUE SIZE
#define OS_APP_CONTROL_PARAMS_QUEUE_SIZE (5)
#define OS_APP_CONTROL_STOP_QUEUE_SIZE (1) //used as semaphore

/********************** SYS CONFIG PARAMS ***************************************/

#if 0
//SYS CONNECTION CONFIG
#define OS_CONFIG_WIFI_SSID ("Fibertel Heredia  2.4GHz")
#define OS_CONFIG_WIFI_PASSWORD ("0123456789")

//SYS ACCESSPOINT CONFIG
#define OS_CONFIG_AP_SSID "dip_ap"
#define OS_CONFIG_AP_PASS "012345678"

//SYS TCP CONFIG (in connection or accesspoint)
#define OS_CONFIG_TCP_ENABLE   			(true)

//SYS OTA UPDATE CONFIG
#define OS_CONFIG_SYSUPDATE_OTA_ENABLE   	(true)

#define OS_CONFIG_SYSUPDATE_OTA_PORT  (8266)
#define OS_CONFIG_SYSUPDATE_OTA_HOST ("myesp32")

#define OS_CONFIG_SYSUPDATE_SD_ENABLE 		(false) //DON'T ENABLE
#define OS_CONFIG_SYSUPDATE_SD_BINARY 		("/firmware.bin")

#endif

#endif /* _COMPONENTS_CONFIG_INCLUDE_OS_H_ */
/** @}Final Doxygen */
