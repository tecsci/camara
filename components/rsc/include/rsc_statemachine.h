/* 
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* Copyright (C) <2021>  <Martin Abel Gambarotta   magambarotta@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		rsc_statemachine.h
* @date		14 jun. 2021
* @author	
*		-martin
* @version	v1.0.0
* @brief
*
* @{ Init Doxygen
*/

#ifndef COMPONENTS_RSC_INCLUDE_RSC_STATEMACHINE_H_
#define COMPONENTS_RSC_INCLUDE_RSC_STATEMACHINE_H_
/********************** inclusions *******************************************/
#include "stdbool.h"
#include "stddef.h"
/********************** macros ***********************************************/

/********************** typedef **********************************************/

typedef struct rsc_statemachine_s rsc_statemachine_t;

typedef void (*rsc_statemachine_handler_t)(rsc_statemachine_t*);

struct rsc_statemachine_s {
	int flagEntry;
	int flagExit;
	rsc_statemachine_handler_t last;
	rsc_statemachine_handler_t next;
	rsc_statemachine_handler_t current;
};


/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

bool rsc_statemachine_is_entry(rsc_statemachine_t *pState);
bool rsc_statemachine_is_exit(rsc_statemachine_t *pState);
void rsc_statemachine_go_to(rsc_statemachine_t *pState, rsc_statemachine_handler_t next);
void rsc_statemachine_init(rsc_statemachine_t *pState, rsc_statemachine_handler_t next);
void rsc_statemachine_state(rsc_statemachine_t *pState);


#endif /* COMPONENTS_RSC_INCLUDE_RSC_STATEMACHINE_H_ */
/** @}Final Doxygen */
