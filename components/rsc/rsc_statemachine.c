/*
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* Copyright (C) <2021>  <Martin Abel Gambarotta   magambarotta@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		rsc_statemachine.c
* @date		14 jun. 2021
* @author	
*		-Martin Abel Gambarotta   magambarotta@gmail.com
* @version	v1.0.0
* @brief
*
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "rsc_statemachine.h"
/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/
bool rsc_statemachine_is_entry(rsc_statemachine_t *sm) {
	return (sm->last != sm->current) ? true : false;
}

bool rsc_statemachine_is_exit(rsc_statemachine_t *sm) {
	return (sm->next != sm->current) ? true : false;
}

void rsc_statemachine_go_to(rsc_statemachine_t *sm, rsc_statemachine_handler_t next) {
	sm->next = next;
}

void rsc_statemachine_init(rsc_statemachine_t *sm, rsc_statemachine_handler_t next) {
	sm->last = NULL;
	sm->current = NULL;
	sm->next = next;
}

void rsc_statemachine_state(rsc_statemachine_t *sm) {
	sm->current = sm->next;
	sm->current(sm);
	sm->last = sm->current;
}


/********************** end of file ******************************************/

/** @}Final Doxygen */
