/*
 * <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
 * 			Copyright (C) <2021>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file		main.c
 * @date		15 A. 2022
 * @author
 *		-Lucas Mancini
 * @version	v1.0.0
 *
 * @brief
 * @{ Init Doxygen
 */

/********************** inclusions *******************************************/

#include "main.h"
#include "app_test.h"
#include "app_console.h"
#include "app_control.h"

#include "board.h"
#include "api.h"
#include "api_bosch_bme280.h"

#if 0
//Test Reset Timer
#include "esp_timer.h"
#include "xtensa/core-macros.h"
#include "esp_log.h"
#endif

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

void app_main(void) {

	//Init Framework
	board_init();
	api_init();
	api_bosch_bme280_init();

	//Init Task
	app_test_init();
	app_console_init();
	app_control_init();

	for (;;) {

#if 0
    	ESP_LOGI("app_principal\r\n");
    	uint32_t time2=esp_log_timestamp();
    	ESP_LOGI("app_principal:", "Time2 -> %u",time2);
#endif

		board_loop();
		api_loop();
		api_bosch_bme280_loop();

		vTaskDelay(1000 / portTICK_PERIOD_MS);
	}
}

/********************** external functions definition ************************/

/********************** end of file ******************************************/
/** @}Final Doxygen */

